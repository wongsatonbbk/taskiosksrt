unit UfrmADOInput;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AdvGlassButton, Vcl.StdCtrls,
  CurvyControls, Vcl.Imaging.pngimage, Vcl.ExtCtrls, se_image, Data.DB, MemDS,
  DBAccess, Uni;

type
  TfrmADOInput = class(TForm)
    cpInput: TCurvyPanel;
    tInputTitle: TLabel;
    iLogo: TseImage;
    qFree: TUniQuery;
    cpProduct0: TCurvyPanel;
    bInputBack3: TAdvGlassButton;
    bProductB7Fact: TAdvGlassButton;
    bProductB10: TAdvGlassButton;
    bProductB7: TAdvGlassButton;
    bProductB10Fact: TAdvGlassButton;
    procedure bInputBackClick(Sender: TObject);
    procedure bInputOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmADOInput: TfrmADOInput;

implementation

uses
  UfrmMain;

{$R *.dfm}

procedure TfrmADOInput.bInputBackClick(Sender: TObject);
begin
  frmMain.ReturnI := False;;
  frmMain.InputOpening := False;
  Self.Release;
end;

procedure TfrmADOInput.bInputOKClick(Sender: TObject);
begin
  //Set Default
  frmMain.ReturnI := False;

  frmMain.arValInput[3] := (Sender as TAdvGlassButton).Tag.ToString;
  frmMain.ReturnI := True;
  frmMain.InputOpening := False;

  Self.Release;
end;

procedure TfrmADOInput.FormShow(Sender: TObject);
begin
  // eInput.Text := EmptyStr;
  frmMain.InputOpening := True;

  Self.Top := 275;
  Self.Width := 1342;
  Self.Height := 678;

  case Self.Tag of // DO
    1: //B7
      begin
        bProductB7.Top := 9;
        bProductB7Fact.Top := 113;
        bProductB7.Visible := True;
        bProductB7Fact.Visible := True;
        bProductB10.Visible := False;
        bProductB10Fact.Visible := False;
        tInputTitle.Caption := frmMain.ReadMessage(62);
        // tProduct1Comp.Caption := frmMain.CompSEQ.ToString;
      end;
    2: //B10
      begin
        bProductB10.Top := 9;
        bProductB10Fact.Top := 113;
        bProductB7.Visible := False;
        bProductB7Fact.Visible := False;
        bProductB10.Visible := True;
        bProductB10Fact.Visible := True;
        tInputTitle.Caption := frmMain.ReadMessage(63);
        // tProduct1Comp.Caption := frmMain.CompSEQ.ToString;
      end;
  end;
//   ShowMessage(Self.Tag.ToString);
end;

end.
