unit UfrmListData;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, MemDS, DBAccess, Uni,
  AdvGlassButton, Vcl.StdCtrls, CurvyControls, AdvUtil, Vcl.Grids, AdvObj,
  BaseGrid, AdvGrid, DBAdvGrid, AdvPicture, DBAdvPicture;

type
  TfrmListData = class(TForm)
    cpInput: TCurvyPanel;
    tInputTitle: TLabel;
    cpList: TCurvyPanel;
    qDriver: TUniQuery;
    cpDriverPict: TCurvyPanel;
    DBAdvPictureDriverPict: TDBAdvPicture;
    cpOrderDetail: TCurvyPanel;
    grdData: TDBAdvGrid;
    sDriver: TUniDataSource;
    bConfirm: TAdvGlassButton;
    qTruck: TUniQuery;
    sTruck: TUniDataSource;
    procedure bInputBack3Click(Sender: TObject);
    procedure bConfirmClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmListData: TfrmListData;

implementation

uses
  UfrmMain;

{$R *.dfm}

procedure TfrmListData.bConfirmClick(Sender: TObject);
begin
  try
    case Self.Tag of
      // Driver Data
      0:
        begin
          frmMain.sDriverID := qDriver.FieldByName('dvID').AsString;
          frmMain.DriverIDCard := qDriver.FieldByName('dvIDCard').AsString;
          frmMain.ReturnI := True;
        end;
      // Truck Data
      1:
        begin
          frmMain.sTruckID := qTruck.FieldByName('trID').AsString;
          frmMain.sTruckPlate := qTruck.FieldByName('trName').AsString;
          frmMain.ReturnI := True;
        end;
    end;
  finally
    frmMain.ListOpening := False;
    Self.Release;
  end;
end;

procedure TfrmListData.bInputBack3Click(Sender: TObject);
begin
  frmMain.ListOpening := False;
  Self.Release;
end;

procedure TfrmListData.FormShow(Sender: TObject);
begin
  case Self.Tag of
    // Driver List
    0:
      begin
        cpDriverPict.Visible := True;
        tInputTitle.Caption := '�����ž�ѡ�ҹ�Ѻö';
        grdData.DataSource := sDriver;
        grdData.Columns[1].FieldName := 'dvName';
        grdData.Columns[2].FieldName := 'dvIDCard';

        with qDriver do
        begin
          Close;
          SQL.Text := 'SELECT * FROM vDriver WHERE dvIDCard LIKE ''%' +
            frmMain.DriverIDCard + ''' and dvMJO = 2';
          Open;
        end;
      end;

    // Truck List
    1:
      begin
        cpDriverPict.Visible := False;
        tInputTitle.Caption := '������ö��÷ء';
        grdData.DataSource := sTruck;
        grdData.Columns[1].FieldName := 'trName';
        grdData.Columns[2].FieldName := 'trTotalVolume2';

        with qTruck do
        begin
          Close;
          SQL.Text := 'SELECT *, Format(trTotalVolume, ''###,###'') + '' L.'' AS trTotalVolume2 FROM vTruck WHERE trLicensePlate LIKE ''' +
            frmMain.sTruckPlate + '%'' and trMajorOil = 2';
          Open;
        end;
      end;
  end;
end;

end.
