object frmHelp: TfrmHelp
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'frmHelp'
  ClientHeight = 879
  ClientWidth = 1288
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cpInput: TCurvyPanel
    Left = 0
    Top = 0
    Width = 1288
    Height = 879
    Align = alClient
    Rounding = 15
    TabOrder = 0
    object tInputTitle: TLabel
      AlignWithMargins = True
      Left = 10
      Top = 10
      Width = 1268
      Height = 60
      Margins.Left = 10
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 0
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = #3623#3636#3608#3637#3585#3634#3619#3629#3629#3585#3605#3633#3659#3623#3650#3604#3618#3651#3594#3657#3648#3588#3619#3639#3656#3629#3591#3588#3637#3629#3629#3626
      Color = 4342527
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
      Layout = tlCenter
      ExplicitTop = -1
    end
    object cpOrderDetail: TCurvyPanel
      AlignWithMargins = True
      Left = 10
      Top = 80
      Width = 1268
      Height = 676
      Margins.Left = 10
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 0
      Align = alClient
      BorderColor = clSilver
      TabOrder = 0
      object wbHelp: TWebBrowser
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 1262
        Height = 670
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 10
        ExplicitTop = -340
        ExplicitHeight = 528
        ControlData = {
          4C0000006E8200003F4500000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E12620F000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
      object AcroPDF1: TAcroPDF
        Left = 0
        Top = 0
        Width = 1268
        Height = 676
        Align = alClient
        TabOrder = 1
        ExplicitLeft = 158
        ExplicitTop = 43
        ControlData = {000E00000D830000DE450000}
      end
    end
    object cpConfirm: TCurvyPanel
      AlignWithMargins = True
      Left = 15
      Top = 766
      Width = 1258
      Height = 103
      Margins.Left = 15
      Margins.Top = 10
      Margins.Right = 15
      Margins.Bottom = 10
      Align = alBottom
      BorderColor = clNone
      Rounding = 15
      TabOrder = 1
      ExplicitLeft = 14
      DesignSize = (
        1258
        103)
      object bInputBack: TAdvGlassButton
        Left = 1033
        Top = 4
        Width = 223
        Height = 98
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        AntiAlias = aaNone
        BackColor = 7194872
        Caption = #3585#3621#3633#3610
        CornerRadius = 10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -51
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ForeColor = clWhite
        GlowColor = 2731765
        InnerBorderColor = clNone
        OuterBorderColor = clWhite
        ParentFont = False
        ShineColor = 2731765
        TabOrder = 0
        Version = '1.3.0.2'
        OnClick = bInputBackClick
      end
      object bPrevious: TAdvGlassButton
        Left = 3
        Top = 3
        Width = 223
        Height = 98
        Cursor = crHandPoint
        AntiAlias = aaNone
        BackColor = clGreen
        Caption = #3618#3657#3629#3609#3627#3621#3633#3591
        CornerRadius = 10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -51
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ForeColor = clWhite
        GlowColor = clGreen
        InnerBorderColor = clNone
        OuterBorderColor = clWhite
        ParentFont = False
        ShineColor = clGreen
        TabOrder = 1
        Version = '1.3.0.2'
        OnClick = bPreviousClick
      end
      object bNext: TAdvGlassButton
        Left = 234
        Top = 2
        Width = 223
        Height = 98
        Cursor = crHandPoint
        AntiAlias = aaNone
        BackColor = 13323073
        Caption = #3605#3656#3629#3652#3611
        CornerRadius = 10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -51
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ForeColor = clWhite
        GlowColor = 13323073
        InnerBorderColor = clNone
        OuterBorderColor = clWhite
        ParentFont = False
        ShineColor = 13323073
        TabOrder = 2
        Version = '1.3.0.2'
        OnClick = bNextClick
      end
    end
  end
end
