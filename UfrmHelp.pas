unit UfrmHelp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, CurvyControls,
  Vcl.OleCtrls, SHDocVw, System.Win.Registry, AdvGlassButton, AcroPDFLib_TLB;

type
  TfrmHelp = class(TForm)
    cpInput: TCurvyPanel;
    tInputTitle: TLabel;
    cpOrderDetail: TCurvyPanel;
    wbHelp: TWebBrowser;
    cpConfirm: TCurvyPanel;
    bInputBack: TAdvGlassButton;
    AcroPDF1: TAcroPDF;
    bPrevious: TAdvGlassButton;
    bNext: TAdvGlassButton;
    procedure FormCreate(Sender: TObject);
    procedure bInputBackClick(Sender: TObject);
    procedure bNextClick(Sender: TObject);
    procedure bPreviousClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetPermissions;
    { Public declarations }

  const
    cHomePath = 'SOFTWARE';
    cFeatureBrowserEmulation =
      'Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION\';
    cIE11 = 11001;
  end;

var
  frmHelp: TfrmHelp;

implementation

{$R *.dfm}

procedure TfrmHelp.bInputBackClick(Sender: TObject);
begin
  Self.Hide;
end;

procedure TfrmHelp.bNextClick(Sender: TObject);
begin
  AcroPDF1.gotoNextPage;
end;

procedure TfrmHelp.bPreviousClick(Sender: TObject);
begin
  AcroPDF1.gotoPreviousPage;
end;

procedure TfrmHelp.FormCreate(Sender: TObject);
begin
  // SetPermissions;

  AcroPDF1.src := ExtractFileDir(ParamStr(0)) +
    '\Help\�Ըա���͡������������ͧ�����.pdf';
end;

procedure TfrmHelp.SetPermissions;
var
  Reg: TRegIniFile;
  sKey: string;
begin
  sKey := ExtractFileName(ParamStr(0));
  Reg := TRegIniFile.Create(cHomePath);
  try
    if Reg.OpenKey(cFeatureBrowserEmulation, True) and
      not(TRegistry(Reg).KeyExists(sKey) and (TRegistry(Reg).ReadInteger(sKey)
      = cIE11)) then
      TRegistry(Reg).WriteInteger(sKey, cIE11);
  finally
    Reg.Free;
  end;
end;

end.
