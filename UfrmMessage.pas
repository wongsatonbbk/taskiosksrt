unit UfrmMessage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AdvGlassButton, Vcl.StdCtrls,
  CurvyControls;

type
  TfrmMessage = class(TForm)
    cpMessage: TCurvyPanel;
    tMessageTitle: TLabel;
    tMessageBody: TLabel;
    bMessageOK: TAdvGlassButton;
    procedure bMessageOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMessage: TfrmMessage;

implementation

{$R *.dfm}

procedure TfrmMessage.bMessageOKClick(Sender: TObject);
begin
  Close;
end;

end.
