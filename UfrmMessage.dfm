object frmMessage: TfrmMessage
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'frmMessage'
  ClientHeight = 302
  ClientWidth = 651
  Color = clWhite
  TransparentColorValue = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object cpMessage: TCurvyPanel
    Left = 0
    Top = 0
    Width = 651
    Height = 302
    Align = alClient
    Rounding = 15
    TabOrder = 0
    object tMessageTitle: TLabel
      AlignWithMargins = True
      Left = 10
      Top = 10
      Width = 631
      Height = 60
      Margins.Left = 10
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 0
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = #3649#3592#3657#3591#3648#3605#3639#3629#3609
      Color = 4342527
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
      Layout = tlCenter
      ExplicitWidth = 661
    end
    object tMessageBody: TLabel
      AlignWithMargins = True
      Left = 10
      Top = 75
      Width = 631
      Height = 130
      Margins.Left = 10
      Margins.Top = 5
      Margins.Right = 10
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = #3586#3657#3629#3588#3623#3634#3617'...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -25
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
    end
    object bMessageOK: TAdvGlassButton
      Left = 83
      Top = 204
      Width = 485
      Height = 72
      Cursor = crHandPoint
      AntiAlias = aaNone
      BackColor = 13323073
      BackGroundSymbolColor = clLime
      Caption = #3605#3585#3621#3591
      CornerRadius = 10
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 16718362
      Font.Height = -40
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ForeColor = clWhite
      GlowColor = 13323073
      InnerBorderColor = clNone
      Layout = blGlyphBottom
      OuterBorderColor = clWhite
      ParentFont = False
      ShineColor = clWhite
      TabOrder = 0
      Version = '1.3.1.0'
      OnClick = bMessageOKClick
    end
  end
end
