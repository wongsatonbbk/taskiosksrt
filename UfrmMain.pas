unit UfrmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.jpeg, Vcl.ExtCtrls,
  Vcl.Menus, Vcl.Imaging.pngimage, se_image, AdvGlassButton, Vcl.StdCtrls,
  CurvyControls, StrUtils, AdvSmoothLabel, datelbl, AdvMetroProgressBar,
  AdvProgressBar, AdvUtil, Vcl.Grids, AdvObj, BaseGrid, AdvGrid, DBAdvGrid,
  Data.DB, AdvOfficeTabSet, AdvOfficeTabSetStylers, Vcl.ComCtrls,
  AdvPageControl,
  DBAccess, Uni, MemDS, Vcl.DBCtrls, UniProvider, SQLServerUniProvider,
  AdvPicture, DBAdvPicture, AsgLinks, System.Math, frmctrllink,
  AdvDBLookupComboBox, frxClass, frxExportPDF, frxDBSet, Vcl.WinXCtrls,
  DAAlerter, UniAlerter, ipwcore, ipwipinfo, frxDMPExport, AdvScrollBox,
  propscrl, AdvSmoothScrollBar, Vcl.Buttons, System.Actions, Vcl.ActnList,
  Vcl.Touch.GestureMgr, AdvGDIPicture, DBAdvGDIPPicture, DASQLMonitor,
  UniSQLMonitor, RzCommon, Winapi.TlHelp32, Winapi.ShellAPI, RzLaunch, DAScript,
  UniScript;

type
  //Message Type
  TTitleType = (tTypeWaring, tTypeError, tTypeMessage);
  //String Array Type
  TStringArray = array of string;

type
  TfrmMain = class(TForm)
    pmExit: TPopupMenu;
    Exit1: TMenuItem;
    cpBG: TCurvyPanel;
    DateLabel1: TDateLabel;
    cp1: TCurvyPanel;
    b1: TAdvGlassButton;
    b2: TAdvGlassButton;
    b3: TAdvGlassButton;
    b4: TAdvGlassButton;
    b5: TAdvGlassButton;
    b6: TAdvGlassButton;
    b7: TAdvGlassButton;
    b8: TAdvGlassButton;
    b9: TAdvGlassButton;
    b0: TAdvGlassButton;
    bDelete: TAdvGlassButton;
    bClear: TAdvGlassButton;
    bConfirm: TAdvGlassButton;
    bNeg: TAdvGlassButton;
    bSlash: TAdvGlassButton;
    t3: TLabel;
    t4: TLabel;
    i1: TseImage;
    pgOrder: TAdvProgressBar;
    cpMain: TCurvyPanel;
    AdvPageControlMain: TAdvPageControl;
    tsDriver: TAdvTabSheet;
    cpDriverandTruck: TCurvyPanel;
    tCaptionFirst: TLabel;
    tCaptionFirst2: TLabel;
    eSSID: TCurvyEdit;
    tsAssingment: TAdvTabSheet;
    Order: TCurvyPanel;
    cpOrderHead: TCurvyPanel;
    t7: TLabel;
    grdOrderHeader: TDBAdvGrid;
    cpOrderDetail: TCurvyPanel;
    t8: TLabel;
    grdOrderDetail: TDBAdvGrid;
    cp15: TCurvyPanel;
    t5: TLabel;
    grdOrderComp: TDBAdvGrid;
    img_BG: TImage;
    qDriver: TUniQuery;
    sDriver: TUniDataSource;
    dbtxtdvName: TDBText;
    SQLServerUniProvider: TSQLServerUniProvider;
    qTruck: TUniQuery;
    sTruck: TUniDataSource;
    qTruckComp: TUniQuery;
    sTruckComp: TUniDataSource;
    qOrder: TUniQuery;
    sOrder: TUniDataSource;
    sOrderComp: TUniDataSource;
    qOrderComp: TUniQuery;
    qOrderDetailHeader: TUniQuery;
    sOrderDetailHeader: TUniDataSource;
    sOrderDetail: TUniDataSource;
    qOrderDetail: TUniQuery;
    qFree: TUniQuery;
    UniSQL: TUniSQL;
    qProductSale: TUniQuery;
    sProductSale: TUniDataSource;
    frxDBLoad: TfrxDBDataset;
    frxPDFLAT: TfrxPDFExport;
    frxReportLAT: TfrxReport;
    qOrderDetailPrint: TUniQuery;
    frxDBDetailPrint: TfrxDBDataset;
    cp7: TCurvyPanel;
    bReset1: TAdvGlassButton;
    UniConnectionTAS: TUniConnection;
    idcData: TActivityIndicator;
    mmoLog: TMemo;
    ipwIPInfo1: TipwIPInfo;
    Truck: TAdvTabSheet;
    cp2: TCurvyPanel;
    t9: TLabel;
    t10: TLabel;
    cp3: TCurvyPanel;
    cp5: TCurvyPanel;
    t6: TLabel;
    grdTruckkComp: TDBAdvGrid;
    eTruck: TCurvyEdit;
    cp4: TCurvyPanel;
    AdvPicture1: TAdvPicture;
    qCustomer: TUniQuery;
    sCustomer: TUniDataSource;
    tmrFade: TTimer;
    bTestforDebug: TAdvGlassButton;
    bBack: TAdvGlassButton;
    dbtxttrShow: TDBText;
    mmoComp: TMemo;
    cpAssingTruck: TCurvyPanel;
    AdvPicture2: TAdvPicture;
    dbtxttrShow2: TDBText;
    cpComp1: TCurvyPanel;
    tCompTitle1: TLabel;
    tVolume11: TLabel;
    tVolume12: TLabel;
    tVolume13: TLabel;
    tVolume14: TLabel;
    tVolumeProduct1: TLabel;
    tVolumeProductPreset1: TLabel;
    cpComp2: TCurvyPanel;
    tCompTitle2: TLabel;
    tVolume21: TLabel;
    tVolume22: TLabel;
    tVolume23: TLabel;
    tVolume24: TLabel;
    tVolumeProduct2: TLabel;
    tVolumeProductPreset2: TLabel;
    cpComp3: TCurvyPanel;
    tCompTitle3: TLabel;
    tVolume31: TLabel;
    tVolume32: TLabel;
    tVolume33: TLabel;
    tVolume34: TLabel;
    tVolumeProduct3: TLabel;
    tVolumeProductPreset3: TLabel;
    cpComp4: TCurvyPanel;
    tCompTitle4: TLabel;
    tVolume41: TLabel;
    tVolume42: TLabel;
    tVolume43: TLabel;
    tVolume44: TLabel;
    tVolumeProduct4: TLabel;
    tVolumeProductPreset4: TLabel;
    cpComp5: TCurvyPanel;
    tCompTitle5: TLabel;
    tVolume51: TLabel;
    tVolume52: TLabel;
    tVolume53: TLabel;
    tVolume54: TLabel;
    tVolumeProduct5: TLabel;
    tVolumeProductPreset5: TLabel;
    cpComp6: TCurvyPanel;
    tCompTitle6: TLabel;
    tVolume61: TLabel;
    tVolume62: TLabel;
    tVolume63: TLabel;
    tVolume64: TLabel;
    tVolumeProduct6: TLabel;
    tVolumeProductPreset6: TLabel;
    cpComp7: TCurvyPanel;
    tCompTitle7: TLabel;
    tVolume71: TLabel;
    tVolume72: TLabel;
    tVolume73: TLabel;
    tVolume74: TLabel;
    tVolumeProduct7: TLabel;
    tVolumeProductPreset7: TLabel;
    cpComp8: TCurvyPanel;
    tCompTitle8: TLabel;
    tVolume81: TLabel;
    tVolume82: TLabel;
    tVolume83: TLabel;
    tVolume84: TLabel;
    tVolumeProduct8: TLabel;
    tVolumeProductPreset8: TLabel;
    cpComp9: TCurvyPanel;
    tCompTitle9: TLabel;
    tVolume91: TLabel;
    tVolume92: TLabel;
    tVolume93: TLabel;
    tVolume94: TLabel;
    tVolumeProduct9: TLabel;
    tVolumeProductPreset9: TLabel;
    cpComp10: TCurvyPanel;
    tCompTitle10: TLabel;
    tVolume101: TLabel;
    tVolume102: TLabel;
    tVolume103: TLabel;
    tVolume104: TLabel;
    tVolumeProduct10: TLabel;
    tVolumeProductPreset10: TLabel;
    t16: TLabel;
    cpBGComp: TCurvyPanel;
    frxDBHead: TfrxDBDataset;
    frxDBCustomer: TfrxDBDataset;
    CurvyPanel1: TCurvyPanel;
    eSealNumber: TCurvyEdit;
    AdvGlassButton1: TAdvGlassButton;
    bAddOrderClick2: TAdvGlassButton;
    bReload: TAdvGlassButton;
    bUpdate: TAdvGlassButton;
    frxDBComp: TfrxDBDataset;
    UniQuery1: TUniQuery;
    frxDBDataset1: TfrxDBDataset;
    frxDBDetail: TfrxDBDataset;
    tmrGrdBlink: TTimer;
    pnlScrlOD: TPanel;
    btnODUp: TSpeedButton;
    btnODDown: TSpeedButton;
    pnlScrlOH: TPanel;
    btnOHUp: TSpeedButton;
    btnOHDown: TSpeedButton;
    DBAdvPictureDriverPict: TDBAdvGDIPPicture;
    cpProduct0: TCurvyPanel;
    Label1: TLabel;
    gpnlProduct0: TGridPanel;
    bProduct11: TAdvGlassButton;
    bProduct12: TAdvGlassButton;
    bProduct13: TAdvGlassButton;
    bProduct14: TAdvGlassButton;
    bProduct15: TAdvGlassButton;
    bProduct16: TAdvGlassButton;
    bProduct17: TAdvGlassButton;
    bProduct1Pass: TAdvGlassButton;
    cpProduct1: TCurvyPanel;
    Label2: TLabel;
    gpnlProduct1: TGridPanel;
    bProduct01: TAdvGlassButton;
    bProduct02: TAdvGlassButton;
    bProduct03: TAdvGlassButton;
    bProduct04: TAdvGlassButton;
    bProduct05: TAdvGlassButton;
    bProduct06: TAdvGlassButton;
    bProduct07: TAdvGlassButton;
    bProduct0Pass: TAdvGlassButton;
    Label3: TLabel;
    Label4: TLabel;
    UniSQLMonitor: TUniSQLMonitor;
    RzRegIniFile: TRzRegIniFile;
    RzPropertyStoreDB: TRzPropertyStore;
    Button1: TButton;
    eCard: TCurvyEdit;
    bCreateTicketByCard: TAdvGlassButton;
    RzLauncher1: TRzLauncher;
    procedure Exit1Click(Sender: TObject);
    procedure b1Click(Sender: TObject);
    procedure bDeleteClick(Sender: TObject);
    procedure bClearClick(Sender: TObject);
    procedure bTruck1Click(Sender: TObject);
    procedure bTruckDelClick(Sender: TObject);
    procedure bTruckClearClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bOrderBackClick(Sender: TObject);
    procedure bCustConfirmClick(Sender: TObject);
    procedure bResetClick(Sender: TObject);
    procedure bFinishClick(Sender: TObject);
    procedure eOrderClick(Sender: TObject);
    procedure eCustClick(Sender: TObject);
    procedure bOil1Click(Sender: TObject);
    procedure bAddOilClick(Sender: TObject);
    procedure eSSIDChange(Sender: TObject);
    procedure bConfirmClick(Sender: TObject);
    procedure grdOrderHeaderClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure bAddOrderClick(Sender: TObject);
    procedure eSealNumberClick(Sender: TObject);
    procedure bConfirmSealCustTypeClick(Sender: TObject);
    procedure bReset1Click(Sender: TObject);
    procedure UniSQLAfterExecute(Sender: TObject; Result: Boolean);
    procedure UniSQLBeforeExecute(Sender: TObject);
    procedure qOrderBeforeFetch(DataSet: TCustomDADataSet; var Cancel: Boolean);
    procedure qOrderAfterFetch(DataSet: TCustomDADataSet);
    procedure eCardControlClick(Sender: TObject; Index: Integer);
    procedure bRetailClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure tmrFadeTimer(Sender: TObject);
    procedure AdvPageControlMainCanChange(Sender: TObject;
      FromPage, ToPage: Integer; var AllowChange: Boolean);
    procedure bTestforDebugClick(Sender: TObject);
    procedure bBackClick(Sender: TObject);
    procedure bReloadClick(Sender: TObject);
    procedure tVolume11Click(Sender: TObject);
    procedure cpComp1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure bUpdateClick(Sender: TObject);
    procedure tmrGrdBlinkTimer(Sender: TObject);
    procedure bProduct11Click(Sender: TObject);
    procedure btnODUpClick(Sender: TObject);
    procedure btnODDownClick(Sender: TObject);
    procedure btnOHUpClick(Sender: TObject);
    procedure btnOHDownClick(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure UniConnectionTASError(Sender: TObject; E: EDAError;
      var Fail: Boolean);
    procedure eTestCardChange(Sender: TObject);
    procedure bCreateTicketByCardClick(Sender: TObject);
  private
    slMessage : TStringList;
    GridIndexE, SendTag: ShortInt;
    CustOrOrder, DriverOrTruck, SealOrGrid: Boolean;
    OilName, wksIP, wksName: string;

    procedure ProgressOrder(SEQ: Smallint);
    procedure AutoSelectGenOrderDCustomer(Order: string;
      const DriverID: Integer = 0; const TruckID: Integer = 0);
    function eSQL(const SQL: String): Boolean;
    function GetEOFSelect(CommandSQL: string): Boolean;
    procedure ev_insert(EV_CODE: Integer; EV_IP, EV_DETAIL: string);

    // First Record
    function IsFirstRecord(ADataSet: TDataSet): Boolean;

    function chkOrderCompNonN: Boolean;
    procedure createOrderComp(bTruckDup: Boolean);

    //Get Master Data
    procedure GetDriver(SSID: string);
    procedure GetTruck(TruckLicense: string);

    { Private declarations }
  public
    PWidth, PHeight: Smallint;
    CompSEQ, ODetailSEQ: ShortInt;
    arValInput: array [0 .. 6] of Variant;
    ReturnI, InputOpening, ListOpening: Boolean;
    FlagFade: Boolean;
    sDriverID, DriverIDCard, sTruckID, sTruckPlate, ODetailProName: string;

    function SelectReturnData(CommandText, FieldReturn: string): string;
    function SelectReturnDataAr(CommandText :string; FieldReturn: TStringArray; const Length: ShortInt = 1): TStringArray;

    procedure AllSumOrderDetail(OrderQ, HeaderQ, OrderDetailQ: TUniQuery);
    procedure ShowPanelMessage(TitleType: TTitleType; Text: string);
    procedure ReloadAllOrderQ;
    procedure RunTruckComp(CompPanel: ShortInt;
      const ProductAssign: Boolean = False; const CompDup: Boolean = False);

    //Readfile
    function ReadMessage(Index: SmallInt; const LoadMessage: Boolean = False): string;
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  UfrmMessage, UfrmInput, UfrmListData, UfrmHelp, UfrmADOInput;

{$R *.dfm}

procedure TfrmMain.bOrderBackClick(Sender: TObject);
begin
  ProgressOrder(1);
end;

procedure TfrmMain.bProduct11Click(Sender: TObject);
begin
  case (Sender as TAdvGlassButton).Tag of
    1:
      begin
        SendTag := 3;

        frmADOInput := TfrmADOInput.Create(Self);
//        frmADOInput.Left := 115;
//        frmADOInput.Top := 304;
        frmADOInput.Tag := 1;
        frmADOInput.Show;
      end;
    2:
      begin
        SendTag := 3;

        frmADOInput := TfrmADOInput.Create(Self);
//        frmADOInput.Left := 115;
//        frmADOInput.Top := 304;
        frmADOInput.Tag := 2;
        frmADOInput.Show;
      end;
  else
    begin
      SendTag := 2;

      arValInput[SendTag] := (Sender as TAdvGlassButton).Tag.ToString;
      ReturnI := True;
      InputOpening := False;
      gpnlProduct0.Visible := False;

      Self.Activate;
    end;
  end;
end;

procedure TfrmMain.bReloadClick(Sender: TObject);
begin
  ReloadAllOrderQ;

  AllSumOrderDetail(qOrder, qOrderDetailHeader, qOrderDetail);

  ProgressOrder(0);
  RunTruckComp(0);
end;

procedure TfrmMain.bReset1Click(Sender: TObject);
var
  s: string;
begin
  try
    if (qOrder.Active) and (qOrder.FieldByName('orDO').AsString <> EmptyStr)
    then
    begin
      // Cancel Order
      s := 'update tOrder set orStatus = 108, orUser = 99 ';
      s := s + ' where orID = ' + qOrder.FieldByName('orID').AsString;

      eSQL(s);

      // Reset tOrderDetail
      s := 'update tOrderDetail set odDOC_NO = NULL ';
      s := s + ' where odOrder = ' + qOrder.FieldByName('orID').AsString;

      eSQL(s);

      // Reset tOrderComp
      s := 'update tOrderComp set ocDOC_NO = NULL ';
      s := s + ' where ocID in (select ocID from vOrderComp where orDO = ' +
           qOrder.FieldByName('orDO').AsString.QuotedString + ') or ocOrder = ' +
           qOrder.FieldByName('orID').AsString;

      eSQL(s);
    end;
  finally
    qOrder.Close;
    ProgressOrder(0);
  end;
end;

procedure TfrmMain.bResetClick(Sender: TObject);
begin
  ProgressOrder(0);
end;

function TfrmMain.chkOrderCompNonN: Boolean;
var
  s: string;
begin
  s := 'select odID from vOrderDetailER where odOrder = ' +
    qOrder.FieldByName('orID').AsString;
  qFree.Close;
  qFree.SQL.Text := s;
  qFree.Open;

  if qFree.Eof then
  begin
    // LogEvent to SQL
    ev_insert(9949, wksIP,
    'Kiosk - [After chkOrderCompNonN] : Tag - ' + SendTag.ToString + ' : Result - True : ' + s);
    Result := True;
  end
  else
  begin
    // LogEvent to SQL
    ev_insert(9949, wksIP,
    'Kiosk - [After chkOrderCompNonN] : Tag - ' + SendTag.ToString + ' : Result - False : ' + s);
    Result := False;
  end;
end;

procedure TfrmMain.bRetailClick(Sender: TObject);
var
  s: string;
begin
  if (Sender as TAdvGlassButton).Tag = 0 then
  begin
    s := 'SELECT DISTINCT cuCodeEN, cuName, cuIsJobber FROM mCustomer';
    s := s + ' where cuMJO = 2 and cuIsJobber = 0 ' + ' order by cuCodeEN';
  end
  else
  begin
    s := 'SELECT DISTINCT cuCodeEN, cuName, cuIsJobber FROM mCustomer';
    s := s + ' where cuMJO = 2 and cuIsJobber = 1 order by cuCodeEN';
  end;

  with qCustomer do
  begin
    Close;
    SQL.Text := s;
    Open;
  end;
end;

procedure TfrmMain.bCustConfirmClick(Sender: TObject);
begin
  ProgressOrder(4);
end;

procedure TfrmMain.bOil1Click(Sender: TObject);
begin
  OilName := (Sender as TAdvGlassButton).Caption;
end;

procedure TfrmMain.b1Click(Sender: TObject);
begin
  idcData.StartAnimation;

  if AdvPageControlMain.ActivePageIndex in [0, 1] then
  begin
    case AdvPageControlMain.ActivePageIndex of
      0:
        begin
          if eCard.Visible then
          begin
            idcData.StopAnimation;
            Exit;
          end;

          // Driver
          if Length(eSSID.Text) = 3 then
          begin
            eSSID.Text := eSSID.Text + (Sender as TAdvGlassButton).Caption;

            GetDriver(eSSID.Text);
          end;

          if Length(eSSID.Text) >= 4 then
          begin
            idcData.StopAnimation;
            Exit;
          end;

          eSSID.Text := eSSID.Text + (Sender as TAdvGlassButton).Caption;
        end;

      1:
        begin
          eTruck.Text := eTruck.Text + (Sender as TAdvGlassButton).Caption;
          sTruckID := EmptyStr;


          if (Length(eTruck.Text) = 6) or (Length(eTruck.Text) = 13) then
          begin
            GetTruck(eTruck.Text);
          end;
        end;
    end;
  end
  else
  begin
    // if SealOrGrid then
    // begin
    // eSealNumber.Text := eSealNumber.Text + (Sender as TAdvGlassButton).Caption;
    // idcData.StopAnimation;
    // Exit;
    // end
    // else
    begin
      case frmInput.Tag of
        0:
          begin
            if Length(frmInput.eInput.Text) >= 10 then
            begin
              idcData.StopAnimation;
              Exit;
            end;

            frmInput.eInput.Text := frmInput.eInput.Text +
              (Sender as TAdvGlassButton).Caption;
          end;
        1:
          begin
            if Length(frmInput.eInput.Text) >= 7 then
            begin
              idcData.StopAnimation;
              Exit;
            end;

            frmInput.eInput.Text := frmInput.eInput.Text +
              (Sender as TAdvGlassButton).Caption;
          end;
        4:
          begin
            if Length(frmInput.eInput.Text) >= 4 then
            begin
              idcData.StopAnimation;
              Exit;
            end;

            frmInput.eInput.Text := frmInput.eInput.Text +
              (Sender as TAdvGlassButton).Caption;
          end;
        6:
          begin
            if Length(frmInput.eInput.Text) >= 2 then
            begin
              idcData.StopAnimation;
              Exit;
            end;

            frmInput.eInput.Text := frmInput.eInput.Text +
              (Sender as TAdvGlassButton).Caption;
          end;
      end;
    end;
  end;

  idcData.StopAnimation;
end;

procedure TfrmMain.bAddOilClick(Sender: TObject);
begin
  ProgressOrder(3);
end;

procedure TfrmMain.bAddOrderClick(Sender: TObject);
begin
  if (qOrderDetailHeader.RecNo = 1) and (qOrderDetailHeader.FieldByName('odDOC_NO').AsString = EmptyStr) then
  begin
    try
      qOrderDetailHeader.Refresh;
    finally
      qOrderDetailHeader.Last;

      frmInput := TfrmInput.Create(Self);
      frmInput.Tag := 0;
      SendTag := 0;
      frmInput.Show;
    end;
  end else
  begin
    AutoSelectGenOrderDCustomer(qOrder.FieldByName('orID').AsString,
      qOrder.FieldByName('orDriver').AsInteger, qOrder.FieldByName('orTruck')
      .AsInteger);
  end;
end;

procedure TfrmMain.bBackClick(Sender: TObject);
begin
  case AdvPageControlMain.ActivePageIndex of
    0:
      begin
        if eSSID.Visible then
          eSSID.Text := EmptyStr
        else
          eCard.Text := EmptyStr;

        if ListOpening then
        begin
          ListOpening := False;
          frmListData.Tag := -1;
          frmListData.Release;
        end;
      end;
    1:
      begin
        if InputOpening then
        begin
          InputOpening := False;
          frmInput.Tag := -1;
          SendTag := -1;
          frmInput.Release;
        end;

        if ListOpening then
        begin
          ListOpening := False;
          frmListData.Tag := -1;
          SendTag := -1;
          frmListData.Release;
        end;

        bConfirm.Visible := True;
        AdvPageControlMain.ActivePageIndex := 0;
      end;
    2:
      begin
        if InputOpening then
        begin
          InputOpening := False;
          frmInput.Release;
        end;

        if frmInput <> nil then
        frmInput.Tag := -1;

        SendTag := -1;

        bReset1.Click;
        // bConfirm.Visible := True;
        // AdvPageControlMain.ActivePageIndex := 1;
      end;
  end;
end;

procedure TfrmMain.Action1Execute(Sender: TObject);
begin
  Click;
end;

procedure TfrmMain.AdvPageControlMainCanChange(Sender: TObject;
  FromPage, ToPage: Integer; var AllowChange: Boolean);
begin
  AllowChange := False;
end;

procedure TfrmMain.AllSumOrderDetail(OrderQ, HeaderQ, OrderDetailQ: TUniQuery);
//var
//  SumQTY: Integer;
begin
//  with HeaderQ do
//  begin
//    First;
//
//    while not Eof do
//    begin
//      with qFree do
//      begin
//        Close;
//        SQL.Text := 'SELECT * FROM vOrderDetailSUM WHERE  (ocOrder= ' +
//          OrderQ.FieldByName('orID').AsString + ' ) and (ocDOC_NO=' +
//          HeaderQ.FieldByName('odDOC_NO').AsString.QuotedString + ')';
//        Open;
//
//        // (ocProduct=' + OrderDetailQ.FieldByName('odSaleProduct').AsString + ') and
//        while not Eof do
//        begin
//          SumQTY := qFree.FieldByName('ocPreset').AsInteger;
//
//          if OrderDetailQ.Locate('odSaleProduct', FieldByName('ocProduct')
//            .AsInteger, [loPartialKey]) then
//          begin
//            eSQL('update tOrderDetail set odQTY=' + IntToStr(SumQTY) +
//              ' where odID=' + OrderDetailQ.FieldByName('odID').AsString + '');
//
//          end;
//
//          Next;
//        end;
//      end;
//      Next;
//    end;
//  end;

  // Clear Product in tOrderDetail odQTY = 0
  eSQL('update tOrderDetail set odSaleProduct = 0, odQTY = 0 where odID in (select odID from vOrderDetail where odOrder = ' + OrderQ.FieldByName('orID').AsString + ' and ocPreset = 0)');
end;

procedure TfrmMain.AutoSelectGenOrderDCustomer(Order: string;
  const DriverID: Integer = 0; const TruckID: Integer = 0);
var
  i: ShortInt;
  s: string;
begin
//    s := 'insert into tOrderDetail (ORDER_MAIN) values(';
//    s := s + Order;
//    s := s + ')';
//    // LogEvent to SQL
//    ev_insert(9900, wksIP, 'Kiosk - Order [Start OrderHeader] : ' + s);
//    eSQL(s);

  s := EmptyStr;
  // Loop CreateOrderDetail
  for i := 1 to 8 do
  begin
    s := s + 'insert into tOrderDetail (odOrder, odSEQ, odDOC_NO) values(';
    s := s + qOrder.FieldByName('orID').AsString;
    s := s + ',' + IntToStr(i);
    s := s + ', NULL ';// + QuotedStr(qOrderDetailHeader.FieldByName('odDOC_NO').AsString);
    s := s + '); ';
    s := s + ' ';
  end;

  try
    qOrderDetailHeader.Refresh;
  finally
    qOrderDetailHeader.Last;

    frmInput := TfrmInput.Create(Self);
    frmInput.Tag := 0;
    SendTag := 0;
    frmInput.Show;
  end;

  // LogEvent to SQL
  ev_insert(9900, wksIP, 'Kiosk - Order [Start OrderDetail] : ' + s);
  eSQL(s);

  qOrderDetail.Refresh;
end;

procedure TfrmMain.createOrderComp(bTruckDup: Boolean);
var
  s: string;
  trID, OrderStatus: Integer;
begin
  inherited;
  OrderStatus := qOrder.FieldByName('orStatus').AsInteger;

  //IF OrderStatsu <> -99 THEN Create OrderComp
  if OrderStatus <> -99 then
  begin
    // delete existing
    trID := qOrder.FieldValues['orID'];
    s := 'delete tOrderComp WHERE ocOrder = ' + IntToStr(trID) +
      ' and ocDOC_NO = ''0'' and ocProduct = 0 and ocPreset = 0';
    eSQL(s);
    // ShowMessage('1: '+s);

    qFree.Close;

    s := 'SELECT tcCompID, tcVolume1, tcVolume2, tcVolume3, tcVolume4 FROM mTruckComp WHERE tcTUNO = ' +
      qOrder.FieldByName('trTU').AsString;

    qFree.SQL.Text := s;
    qFree.Open;

    // ShowMessage(s);

    while not qFree.Eof do
    begin
      s := 'insert into tOrderComp (ocOrder,ocComp,ocCapacity,tcVolume1,tcVolume2,tcVolume3,tcVolume4) values ( ';
      s := s + IntToStr(qOrder.FieldValues['orID']);
      s := s + ',' + IntToStr(qFree.FieldValues['tcCompID']);
      s := s + ',' + IntToStr(qFree.FieldValues['tcVolume1']);
      s := s + ',' + IntToStr(qFree.FieldValues['tcVolume1']);
      s := s + ',' + IntToStr(qFree.FieldValues['tcVolume2']);
      s := s + ',' + IntToStr(qFree.FieldValues['tcVolume3']);
      s := s + ',' + IntToStr(qFree.FieldValues['tcVolume4']);
      s := s + ')';
      eSQL(s);

      qFree.Next;
    end;

    qFree.Close;

    if qOrderComp.Active then
      qOrderComp.Refresh
    else
      qOrderComp.Open;
  end;
end;

procedure TfrmMain.bClearClick(Sender: TObject);
begin
  case AdvPageControlMain.ActivePageIndex of
    0:
      begin
        if eSSID.Visible then
          eSSID.Text := EmptyStr
        else
          eCard.Text := EmptyStr;

        qDriver.Close;

        if ListOpening then
        begin
          ListOpening := False;
          frmListData.Release;
        end;
      end;
    1:
      begin
        eTruck.Text := EmptyStr;
        qTruck.Close;
      end;
    2:
      begin
        if SealOrGrid then
        begin
          eSealNumber.Text := EmptyStr;
        end
        else
        begin
          if InputOpening then
            frmInput.eInput.Text := EmptyStr;

          case GridIndexE of

            // Clear OrderDetail
            2:
              begin
                try
                  eSQL('update tOrderDetail set odSaleProduct=0, odQTY=0 where odID='
                    + qOrderDetail.FieldByName('odID').AsString + '');
                finally
                  qOrderDetail.Refresh;
                  ProgressOrder(0);
                end;
              end;
            // Clear OrderComp
            3:
              begin
                try
                  eSQL('update tOrderComp set ocDOC_NO=0, ocPreset=0, ocProduct=0 where ocID='
                    + qOrderComp.FieldByName('ocID').AsString + '')
                finally
                  qOrderComp.Refresh;
                  ProgressOrder(0);
                end;
              end;
          end;
        end;
      end;
  end;
end;

procedure TfrmMain.bConfirmClick(Sender: TObject);
var
  s, orIDX, sReportURL: string;
begin
  case AdvPageControlMain.ActivePageIndex of
    0:
      begin
        if ListOpening then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(9));
          Exit;
        end;

        if (eSSID.Text = EmptyStr) or (sDriverID = EmptyStr) then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(10));
          Exit;
        end;

        // Null
        if GetEOFSelect
          ('select orID from vOrderToday Where orStatus = -99 and orMJO = 2 and orDriver = '
          + sDriverID) then
        begin
          AdvPageControlMain.ActivePageIndex := 1;
        end
        else
        begin
          with qOrder do
          begin
            Close;
            s := 'SELECT orID, orIDRef, orDO, orDriver, orTruck, orStatus, trTU, orSeal3, orCustomer, trName + '' - '' + OrderSumLitTXT + '' L.'' AS trNameSUM, trName, dvFullName ' +
                 ' FROM vOrder where orID in (select orID from vOrderToday Where orStatus in (-9, -99) and orDriver = '
                  + sDriverID + ')';
            SQL.Text := s;
            Open;

            if not Eof then
            begin
              if qOrderDetailHeader.Active then
                qOrderDetailHeader.Refresh
              else
                qOrderDetailHeader.Open;

              if qOrderDetail.Active then
                qOrderDetail.Refresh
              else
                qOrderDetail.Open;

              if qOrderComp.Active then
                qOrderComp.Refresh
              else
                qOrderComp.Open;

              ProgressOrder(0);
              GridIndexE := 2;
              AdvPageControlMain.ActivePageIndex := 2;

              tmrGrdBlink.Enabled := True;

              //Comment 03-07-2020
              {
              if qOrderHeader.FieldByName('DO_NO').AsString = EmptyStr then
              begin
                frmInput := TfrmInput.Create(Self);
                frmInput.Tag := 0;
                SendTag := 0;
                frmInput.Show;
              end
              else if qOrderHeader.FieldByName('SHIP_TO').AsString = EmptyStr
              then
              begin
                frmInput := TfrmInput.Create(Self);
                frmInput.Tag := 1;
                SendTag := 1;
                frmInput.Show;
              end
              else
              begin
                if qOrderComp.Locate('ocPreset', 0, []) then
                  CompSEQ := qOrderComp.FieldByName('ocComp').AsInteger;
              end;
              }

              RunTruckComp(0);

            end
            else
            begin
              ShowPanelMessage(tTypeError,
                ReadMessage(11));
            end;
          end;
        end;

        if sDriverID = EmptyStr then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(12));
          Exit;
        end;

        // bConfirm.Visible := False;
      end;

    1:
      begin
        if eTruck.Text = EmptyStr then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(13));
          Exit;
        end;

        if sDriverID = EmptyStr then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(14));
          Exit;
        end;

        mmoComp.Lines.Insert(0, 'sTruckID - ' + sTruckID);

        if sTruckID = EmptyStr then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(15));
          Exit;
        end;

        try
          orIDX := FormatDatetime('yymmddhhnnsszzz', now);

          s := 'insert into tOrder (orIDREF,orMJO,orPIN,orDate,orWorkStation,orDispatcherName,orIDX,orDO, orUser, orDriver, orTruck, orDriverIDCard) values(';
          s := s + '-1';
          s := s + ',2'; //MajorOil
          s := s + ',' + IntToStr(RandomRange(100, 999));
          s := s + ',' + FormatDatetime('YYYYMMDD', Date);
          // ',(SELECT setWorkingDate FROM mSetting)';
          s := s + ',0'; // + QuotedStr(UserSession.wksIP);
          s := s + ',99'; // + QuotedStr(UserSession.wksUser);
          s := s + ',' + QuotedStr(orIDX);
          s := s + ',' + QuotedStr(''); // DO
          s := s + ',0'; // + UserSession.wksUID.ToString; //User
          s := s + ',' + sDriverID;
          s := s + ',' + sTruckID;
          s := s + ',' + QuotedStr(DriverIDCard);
          s := s + ')';


          // LogEvent to SQL
          ev_insert(9900, wksIP, 'Kiosk - Order [Start Order] : ' + s);
          eSQL(s);

          with qOrder do
          begin
            Close;
            s := 'SELECT orID, orIDRef, orDO, orDriver, orTruck, orStatus, trTU, orCustomer, orSeal3, trName + '' - '' + OrderSumLitTXT + '' L.'' AS trNameSUM, trName, dvFullName FROM vOrder where orIDX = '
              + QuotedStr(orIDX);
            SQL.Text := s;
            Open;

            createOrderComp(True);
          end;

          // ProgressValue;

          if qOrderDetailHeader.Active then
            qOrderDetailHeader.Refresh
          else
            qOrderDetailHeader.Open;

          if qOrderDetail.Active then
            qOrderDetail.Refresh
          else
            qOrderDetail.Open;

          AutoSelectGenOrderDCustomer(qOrder.FieldByName('orID').AsString,
            qOrder.FieldByName('orDriver').AsInteger,
            qOrder.FieldByName('orTruck').AsInteger);

          RunTruckComp(0);
        finally
          GridIndexE := 1;
          bConfirm.Visible := False;
          AdvPageControlMain.ActivePageIndex := 2;
        end;
      end;

    2:
      begin
        begin
          if chkOrderCompNonN then
          begin
            bConfirm.Visible := True;
          end;

          if qOrder.FieldByName('orSeal3').AsString = '-' then
          begin
            bConfirm.Visible := False;

            frmInput := TfrmInput.Create(Self);
            frmInput.Tag := 6;
            SendTag := 6;
            frmInput.Show;

            Exit;
          end;

          // Report
          ReloadAllOrderQ;
          qOrderDetail.FilterSQL := 'odSaleProduct <> 0 and odQTY <> 0';

          with qOrderDetailPrint do
          begin
            Close;
            SQL.Text := 'SELECT ROW_NUMBER() OVER(ORDER BY odID ASC) AS Row, * FROM vOrderDetail Where odOrder = ' + qOrder.FieldByName('orID').AsString + ' and odSaleProduct <> 0 and odQTY <> 0 order by odOrder,odSEQ ';
            Open;
          end;

          // {$IFDEF RELEASE}
          sReportURL := ExtractFilePath(ParamStr(0)) +
            'wwwroot\Report\LATKiosk.fr3';
          frxReportLAT.LoadFromFile(sReportURL);
          frxReportLAT.PrepareReport(True);
          frxReportLAT.PrintOptions.ShowDialog := False;
          frxReportLAT.Print;
          // {$ENDIF}

          s := 'update tOrder SET orStatus=-99, orQSEQ=orID WHERE orID=' + qOrder.FieldByName('orID').AsString;
          eSQL(s);
          ev_insert(-99, ipwIPInfo1.AdapterIPAddress, 'KIOSK Print Order : ' +
            qOrder.FieldByName('orID').AsString);

          sDriverID := EmptyStr;
          sTruckID := EmptyStr;
          qOrder.Close;
          ProgressOrder(0);
        end;
      end;
  end;
end;

procedure TfrmMain.bConfirmSealCustTypeClick(Sender: TObject);
begin
  // eSQL('Update tOrder SET orSeal3 = ' + eSealNumber.Text + ' WHERE orID = ' + qOrder.FieldByName('orID').AsString);

  // qOrder.Refresh;

  grdOrderComp.SelectCells(1, 1, 1, 1);
end;

procedure TfrmMain.bCreateTicketByCardClick(Sender: TObject);
begin
  eCard.Text := EmptyStr;
  eSSID.Text := EmptyStr;
  qDriver.Close;

  if not eCard.Visible then
  begin
    bCreateTicketByCard.Caption := ReadMessage(18);
    bCreateTicketByCard.Font.Size := 30;
    tCaptionFirst.Caption := ReadMessage(19);
    tCaptionFirst2.Caption := ReadMessage(20);

    eCard.Visible := True;
    eCard.SetFocus;

    eSSID.Visible := False;
  end else
  begin
    bCreateTicketByCard.Caption := ReadMessage(21);
    bCreateTicketByCard.Font.Size := 22;
    tCaptionFirst.Caption := ReadMessage(22);
    tCaptionFirst2.Caption := ReadMessage(23);

    eCard.Visible := False;
    eSSID.Visible := True;
  end;
end;

procedure TfrmMain.bDeleteClick(Sender: TObject);
var
  s: string;
  SumQTY: Integer;
begin
  case AdvPageControlMain.ActivePageIndex of
    0:
      begin
        if eSSID.Visible then
        begin
          eSSID.Text := LeftStr(eSSID.Text, Length(eSSID.Text) - 1);
        end
        else
        begin
          eCard.Text := LeftStr(eCard.Text, Length(eCard.Text) - 1);
        end;

        if (eSSID.Text = EmptyStr) or (eCard.Text = EmptyStr) then
          qDriver.Close;
      end;
    1:
      begin
        eTruck.Text := LeftStr(eTruck.Text, Length(eTruck.Text) - 1);
      end;
    2:
      begin
        // if SealOrGrid then
        // begin
        // eSealNumber.Text := LeftStr(eSealNumber.Text, Length(eSealNumber.Text) - 1);
        // end
        // else
        // begin

        if InputOpening then
        begin
          frmInput.eInput.Text := LeftStr(frmInput.eInput.Text,
            Length(frmInput.eInput.Text) - 1);
        end
        else
          case GridIndexE of // Clear HeadOrder
            1:
              begin
                try
//                  s := 'update wsDeliveryOrderHeader set DO_NO='''', SHIP_TO='''', TOTAL_WEIGHT=0, VOLUME=0 where ID='
//                    + qOrderDetailHeader.FieldByName('ID').AsString + ';';
//                  eSQL(s);

                  if grdOrderHeader.Row = 1 then
                  begin
                    s := 'update tOrder set orDO='''' where orID=' +
                      qOrder.FieldByName('orID').AsString + '';
                    eSQL(s);
                  end;

                  s := 'update tOrderComp set ocDOC_NO='''', ocProduct=0, ocPreset=0 where ocDOC_NO='
                    + qOrderDetail.FieldByName('odDOC_NO')
                    .AsString.QuotedString + '';
                  eSQL(s);

                  s := 'update tOrderDetail set odDOC_NO=' + QuotedStr('0') + ', odSaleProduct=0, odQTY=0 where odOrder='
                    + qOrderDetailHeader.FieldByName('ID').AsString + '';
                  eSQL(s);

                finally
                  ReloadAllOrderQ;
                  ProgressOrder(0);
                  RunTruckComp(0);
                end;
              end;
            // Clear OrderDetail
            2:
              begin
                try
                  eSQL('update tOrderDetail set odSaleProduct=0, odQTY=0 where odID='
                    + qOrderDetail.FieldByName('odID').AsString + '');
                finally
                  qOrderDetail.Refresh;
                  ProgressOrder(0);
                end;
              end;
            // Clear OrderComp
            3:
              begin
                try
                  // ShowMessage(' Comp :' + qOrderComp.FieldByName('ocProduct').AsString );

                  if qOrderDetail.Locate('odSaleProduct',
                    qOrderComp.FieldByName('ocProduct').AsInteger, []) then
                  begin
                    SumQTY := (qOrderDetail.FieldByName('odQTY').AsInteger -
                      qOrderComp.FieldByName('ocPreset').AsInteger);

                    eSQL('update tOrderDetail set odQTY=' + SumQTY.ToString +
                      ' where odID=' + qOrderDetail.FieldByName('odID')
                      .AsString + '');

                    if SumQTY <> 0 then
                    begin
                      eSQL('UPDATE wsDeliveryOrderHeader SET TOTAL_WEIGHT=(SELECT SUM(tOrderKG) AS tTotalOrderKG FROM vOrderDetail WHERE (odOrder='
                        + qOrderDetailHeader.FieldByName('ID').AsString +
                        ') AND (tOrderKG IS NOT NULL)) WHERE ID=' +
                        qOrderDetailHeader.FieldByName('ID').AsString + '');

                      eSQL('UPDATE wsDeliveryOrderHeader SET VOLUME=(SELECT SUM(odQTY) AS odQTY FROM vOrderDetail WHERE (odOrder='
                        + qOrderDetailHeader.FieldByName('ID').AsString +
                        ') AND (odQTY IS NOT NULL)) WHERE ID=' +
                        qOrderDetailHeader.FieldByName('ID').AsString + '');

                    end
                    else
                    begin
                      eSQL('update tOrderDetail set odSaleProduct=0, odQTY=0 where odID='
                        + qOrderDetail.FieldByName('odID').AsString + '');
                      eSQL('UPDATE wsDeliveryOrderHeader SET TOTAL_WEIGHT=(SELECT SUM(tOrderKG) AS tTotalOrderKG FROM vOrderDetail WHERE (odOrder='
                        + qOrderDetailHeader.FieldByName('ID').AsString +
                        ') AND (tOrderKG IS NOT NULL)) WHERE ID=' +
                        qOrderDetailHeader.FieldByName('ID').AsString + '');

                      eSQL('UPDATE wsDeliveryOrderHeader SET VOLUME=(SELECT SUM(odQTY) AS odQTY FROM vOrderDetail WHERE (odOrder='
                        + qOrderDetailHeader.FieldByName('ID').AsString +
                        ') AND (odQTY IS NOT NULL)) WHERE ID=' +
                        qOrderDetailHeader.FieldByName('ID').AsString + '');
                    end;

                    // eSQL('update tOrderDetail set odQTY=' +  + ' where odID=' + qOrderDetail.FieldByName('odID').AsString + '');
                  end;

                  eSQL('update tOrderComp set ocPreset=0, ocProduct=0 where ocID='
                    + qOrderComp.FieldByName('ocID').AsString + '')
                finally
                  ReloadAllOrderQ;
                  ProgressOrder(0);
                end;
              end;
          end;
        // end;
      end;
  end;
end;

procedure TfrmMain.bFinishClick(Sender: TObject);
begin
  ProgressOrder(0);
end;

procedure TfrmMain.bTestforDebugClick(Sender: TObject);
begin
{$IFDEF DEBUG}
  case AdvPageControlMain.ActivePageIndex of
    0:
      begin
        if eSSID.Visible then
        begin
          eSSID.Text := SelectReturnData
            ('SELECT TOP 1 RIGHT(dvIDCard, 4) as dvIDCard FROM mDriver WHERE dvMJO = 2 ORDER BY NEWID()',
            'dvIDCard');

          with qDriver do
          begin
            Close;
            SQL.Text := 'SELECT dvID, dvIDCard, dvName, dvPict FROM vDriver WHERE dvMJO = 2 and dvIDCard LIKE ''%' +
              eSSID.Text + '''';
            Open;

            if Eof then
            begin
              Close;
              DBAdvPictureDriverPict.Refresh;

              ShowPanelMessage(tTypeError, ReadMessage(24));
            end
            else
            begin
              sDriverID := qDriver.FieldByName('dvID').AsString;
              DriverIDCard := qDriver.FieldByName('dvIDCard').AsString;

              DriverOrTruck := False;
            end;
          end;
        end
        else
        begin
          eCard.Text := SelectReturnData
            ('SELECT TOP 1 orCard FROM vOrderToday WHERE orMJO = 1 and orStatus = -8 ORDER BY NEWID()',
            'orCard');

          if eCard.Text = EmptyStr then
          ShowPanelMessage(tTypeError, ReadMessage(0));
        end;
      end;
    1:
      begin
        eTruck.Text := SelectReturnData
          ('SELECT TOP 1 trName as trName FROM vTruck WHERE trMajorOil = 2 ORDER BY NEWID()',
          'trName');

        with qTruck do
        begin
          Close;
          SQL.Text :=
            'SELECT trID, trName, trCHKExpDate, trStatus, trTU,  trName + '' - '' + Format(trTotalVolume, ''###,###'') + '' L.'' AS trNameSUM FROM vTruck WHERE  trMajorOil = 2 and trName = '
            + QuotedStr(eTruck.Text);
          Open;

          if not Eof then
          begin
            sTruckID := qTruck.FieldByName('trID').AsString;

            qTruckComp.Open;
          end
          else
          begin
            ShowPanelMessage(tTypeError, ReadMessage(0));
          end;
        end;
      end;
    2:
      begin
        // if SealOrGrid then
        // begin
        // eSealNumber.Text := Random(11).ToString;
        // end
        // else
        // begin
        case frmInput.Tag of
          0:
            begin
              frmInput.eInput.Text :=
                SelectReturnData
                ('SELECT FLOOR(RAND()*10000000000) AS DO', 'DO');
            end;
          1:
            begin
              frmInput.eInput.Text :=
                SelectReturnData
                ('SELECT TOP 1 cuCodeEN FROM mCustomer Where cuMJO = 2 ORDER BY NEWID()',
                'cuCodeEN');
            end;
          4:
            begin
              frmInput.eInput.Text :=
                SelectReturnData
                ('SELECT TOP 1 REPLACE(tcVolume1, ''0'', '''') AS tcVolume1 FROM tOrderComp Where ocOrder = '
                + qOrder.FieldByName('orID').AsString + ' ORDER BY NEWID()',
                'tcVolume1');
            end;
          6:
            begin
              frmInput.eInput.Text := Random(11).ToString;
            end;
        else
          Exit;
        end;
        // end;
      end;
  end;
{$ELSE}
  frmHelp.Show;
{$ENDIF}
end;

procedure TfrmMain.btnODDownClick(Sender: TObject);
begin
  qOrderDetail.Next;
end;

procedure TfrmMain.btnODUpClick(Sender: TObject);
begin
  qOrderDetail.Prior;
end;

procedure TfrmMain.btnOHDownClick(Sender: TObject);
begin
  qOrderDetailHeader.Next;
end;

procedure TfrmMain.btnOHUpClick(Sender: TObject);
begin
  qOrderDetailHeader.Prior;
end;

procedure TfrmMain.bTruck1Click(Sender: TObject);
begin
  eTruck.Text := eTruck.Text + (Sender as TAdvGlassButton).Caption;
end;

procedure TfrmMain.bTruckClearClick(Sender: TObject);
begin
  eTruck.Text := EmptyStr;
end;

procedure TfrmMain.bTruckDelClick(Sender: TObject);
begin
  eTruck.Text := LeftStr(eTruck.Text, Length(eTruck.Text) - 1);
end;

procedure TfrmMain.bUpdateClick(Sender: TObject);
begin
  case SendTag of
    0: // Edit DO
      begin
        frmInput := TfrmInput.Create(Self);
        frmInput.eInput.Text := qOrderDetailHeader.FieldByName('odDOC_NO').AsString;
        frmInput.Tag := 0;
        SendTag := 0;
        frmInput.Show;
      end;
    1: // Edit customer
      begin
        frmInput := TfrmInput.Create(Self);
        frmInput.eInput.Text := qOrderDetailHeader.FieldByName('cuCodeEN').AsString;
        frmInput.Left := 115;
        frmInput.Top := 304;
        frmInput.Tag := 1;
        SendTag := 1;
        frmInput.Show;
      end;
    2: // Edit Retail Product
      begin
//        frmInput := TfrmInput.Create(Self);
//        frmInput.Tag := 2;
        SendTag := 2;
        gpnlProduct0.Visible := True;
        gpnlProduct0.BringToFront;
//        frmInput.Show;
      end;
    3: // Edit Jobber Product
      begin
//        frmInput := TfrmInput.Create(Self);
//        frmInput.Tag := 3;
        SendTag := 3;
        gpnlProduct0.Visible := True;
        gpnlProduct0.BringToFront;
//        frmInput.Show;
      end;
    4: // Edit ordercomp QTY
      begin
        frmInput := TfrmInput.Create(Self);
        frmInput.Tag := 4;
        SendTag := 4;
        frmInput.Show;
      end;
  end;
end;

procedure TfrmMain.cpComp1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //
  {
    if not qOrderComp.Active then
    Exit;

    if qOrderComp.Locate('ocComp', (Sender as TCurvyPanel).Tag, []) then
    begin
    CompSEQ := qOrderComp.RecNo;
    GridIndexE := 3;

    qOrderHeader.Locate('DO_NO', qOrderComp.FieldByName('ocDOC_NO').AsString, []);

    frmInput := TfrmInput.Create(Self);

    //Retail Product
    if qOrderHeader.FieldByName('CUST_TYPE').AsInteger = 0 then
    begin
    frmInput.Tag := 2;
    end;

    //Jobber Product
    if qOrderHeader.FieldByName('CUST_TYPE').AsInteger = 1 then
    begin
    frmInput.Tag := 3;
    end;

    frmInput.Show;
    end;

  }

  if not qOrderComp.Active then
    Exit;

  if qOrderComp.Locate('ocComp', (Sender as TCurvyPanel).Tag, []) then
  begin
    CompSEQ := qOrderComp.RecNo;
    ODetailProName := qOrderDetail.FieldByName
      (grdOrderDetail.Columns[3].FieldName).AsString;
    GridIndexE := 3;

    if qOrderDetail.FieldByName('odSaleProduct').AsInteger = 0 then
    begin
      ShowPanelMessage(tTypeError, ReadMessage(25));
      Exit;
    end;

    if qOrderDetail.FieldByName('odQTY').AsInteger = 0 then
    begin
      ShowPanelMessage(tTypeError, ReadMessage(26));
      Exit;
    end;

    frmInput := TfrmInput.Create(Self);

    with frmInput do
    begin
      Tag := 5;
      SendTag := 5;

      with qFree do
      begin
        Close;
        SQL.Text := 'SELECT tcVolume1, tcVolume2, tcVolume3, tcVolume4 FROM vTruckComp WHERE tcTUNO = ' +
          qOrder.FieldByName('trTU').AsString + ' AND tcCompID = ' +
          qOrderComp.FieldByName('ocComp').AsString;
        Open;

        if qFree.FieldValues['tcVolume1'] > 0 then
        begin
          bCompPreset1.Caption := qFree.FieldByName('tcVolume1').AsString;
          bCompPreset1.Tag := qFree.FieldByName('tcVolume1').AsInteger;
          bCompPreset1.Visible := True;
        end;
        if qFree.FieldValues['tcVolume2'] > 0 then
        begin
          bCompPreset2.Caption := qFree.FieldByName('tcVolume2').AsString;
          bCompPreset2.Tag := qFree.FieldByName('tcVolume2').AsInteger;
          bCompPreset2.Visible := True;
        end;
        if qFree.FieldValues['tcVolume3'] > 0 then
        begin
          bCompPreset3.Caption := qFree.FieldByName('tcVolume3').AsString;
          bCompPreset3.Tag := qFree.FieldByName('tcVolume3').AsInteger;
          bCompPreset3.Visible := True;
        end;
        if qFree.FieldValues['tcVolume4'] > 0 then
        begin
          bCompPreset4.Caption := qFree.FieldByName('tcVolume4').AsString;
          bCompPreset4.Tag := qFree.FieldByName('tcVolume4').AsInteger;
          bCompPreset4.Visible := True;
        end;

        qFree.Close;
      end;

      Show;
    end;
  end;
end;

procedure TfrmMain.eCardControlClick(Sender: TObject; Index: Integer);
begin
  case Index of
    0:
    begin
      (Sender as TCurvyEdit).Text := EmptyStr;
      qDriver.Close;
    end;
  end;
end;

procedure TfrmMain.eCustClick(Sender: TObject);
begin
  CustOrOrder := True;
end;

procedure TfrmMain.eOrderClick(Sender: TObject);
begin
  CustOrOrder := False;
end;

procedure TfrmMain.eSealNumberClick(Sender: TObject);
begin
  SealOrGrid := True;
end;

function TfrmMain.eSQL(const SQL: String): Boolean;
begin
  UniSQL.SQL.Text := SQL;
  eSQL := True;
  try
    UniSQL.Execute;
  except
    // lbEvents.Items.Insert(0,'eSQL:' + SQL);
    on E: Exception do
    begin
      ev_insert(9999, wksIP, 'Error CommandSQL ' + SQL + ' | ' + E.Message);

//      ShowMessage(E.Message);
      eSQL := False;
    end;
  end;
end;

procedure TfrmMain.eSSIDChange(Sender: TObject);
begin
  DriverOrTruck := True;
end;

procedure TfrmMain.eTestCardChange(Sender: TObject);
var
  s, sOrIDREF, sOrPIN, orIDX, sOrDriver, sOrTruckNum: string;
  slDupComp: TStringList;
  AssignedComp, TotalComp: ShortInt;
begin
  idcData.StartAnimation;

  if Length(eCard.Text) = 10 then
  begin
    //Check Invalid Card
    if GetEOFSelect('SELECT * FROM mCard WHERE cardCode = ' + QuotedStr(eCard.Text) + '') then
    begin
      ShowPanelMessage(tTypeError, ReadMessage(1));
      idcData.StopAnimation;
      Exit;
    end;

    with qOrder do
    begin
      Close;
      SQL.Text := 'SELECT orID, orIDRef, orDO, orDriver, orTruck, orStatus, trTU, orSeal3, orCustomer, trName + '' - '' + OrderSumLitTXT + '' L.'' AS trNameSUM, trName, dvFullName, orPIN, orDriverIDCard, NumTr ' +
                  ' FROM vOrderToday WHERE orMJO = 1 and orStatus = -8 and orCard=:orCard';
      Params.ParamByName('orCard').AsString := eCard.Text;
      Open;

      if not Eof then
      begin
        sOrIDREF := qOrder.FieldByName('orID').AsString;
        sOrPIN := qOrder.FieldByName('orPIN').AsString;
        sOrDriver := qOrder.FieldByName('orDriverIDCard').AsString;
        sOrTruckNum := qOrder.FieldByName('NumTr').AsString;//AnsiLeftStr(qOrder.FieldByName('trLicensePlate').AsString, 6);

        GetDriver(sOrDriver);
        //Exit from Driver
        if DriverOrTruck = False then
        begin
          idcData.StopAnimation;
          Exit;
        end;

        GetTruck(sOrTruckNum);
        //Exit from Truck
        if DriverOrTruck = False then
        begin
          idcData.StopAnimation;
          Exit;
        end;

        s := 'SELECT COUNT(ocComp) AS ocComp FROM vOrderComp WHERE ocOrder = ' + sOrIDREF + ' and ocProduct <> 0';
        AssignedComp := StrToIntDef(SelectReturnData(s , 'ocComp'), 0);

        s := 'SELECT trTotalComp FROM mTruck WHERE trID = ' + sTruckID;
        TotalComp := StrToIntDef(SelectReturnData(s , 'trTotalComp'), 0);

        if AssignedComp >= TotalComp then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(61));
          idcData.StopAnimation;
          Exit;
        end;

        slDupComp := TStringList.Create;

        try
          if qOrderComp.Active then
            qOrderComp.Refresh
          else
            qOrderComp.Open;

          qOrderComp.FilterSQL := 'ocProduct <> 0';

          while not qOrderComp.Eof do
          begin
            slDupComp.Add(qOrderComp.FieldByName('ocComp').AsString);
            qOrderComp.Next;
          end;

          qOrderComp.FilterSQL := EmptyStr;

          orIDX := FormatDatetime('yymmddhhnnsszzz', now);

          s := 'insert into tOrder (orIDREF,orMJO,orPIN,orDate,orWorkStation,orDispatcherName,orIDX,orDO, orUser, orDriver, orTruck, orDriverIDCard, orCard) values(';
          s := s + sOrIDREF;
          s := s + ', 2'; //MajorOil
          s := s + ',' + sOrPIN;
          s := s + ',' + FormatDatetime('YYYYMMDD', Date);
          // ',(SELECT setWorkingDate FROM mSetting)';
          s := s + ',0'; // + QuotedStr(UserSession.wksIP);
          s := s + ',99'; // + QuotedStr(UserSession.wksUser);
          s := s + ',' + QuotedStr(orIDX);
          s := s + ',' + QuotedStr(''); // DO
          s := s + ',0'; // + UserSession.wksUID.ToString; //User
          s := s + ',' + sDriverID;
          s := s + ',' + sTruckID;
          s := s + ',' + QuotedStr(DriverIDCard);
          s := s + ',' + QuotedStr(eCard.Text);
          s := s + ')';

          // LogEvent to SQL
          ev_insert(9900, wksIP, 'Kiosk - Order [Start Order] : ' + s);
          eSQL(s);

          with qOrder do
          begin
            Close;
            s := 'SELECT orID, orIDRef, orDO, orDriver, orTruck, orStatus, trTU, orSeal3, orCustomer, trName + '' - '' + OrderSumLitTXT + '' L.'' AS trNameSUM, trName, dvFullName FROM vOrder where orIDX = ' + QuotedStr(orIDX);
            SQL.Text := s;
            Open;

            createOrderComp(True);
          end;

          s := 'update tOrder set orIDREF = ' + qOrder.FieldByName('orID').AsString + ' where orID = ' + sOrIDREF;
          // LogEvent to SQL
          ev_insert(9900, wksIP, 'Kiosk - Order [Set Reference Order to AnotherMJO] : ' + s);
          eSQL(s);

          s := 'update tOrder set orIDREF = -1 where orID <> ' + qOrder.FieldByName('orID').AsString +' and orIDREF = ' + sOrIDREF;
          // LogEvent to SQL
          ev_insert(9900, wksIP, 'Kiosk - Order [Cancel Old Reference Order to AnotherMJO] : ' + s);
          eSQL(s);

          if qOrderDetailHeader.Active then
            qOrderDetailHeader.Refresh
          else
            qOrderDetailHeader.Open;

          if qOrderDetail.Active then
            qOrderDetail.Refresh
          else
            qOrderDetail.Open;

          AutoSelectGenOrderDCustomer(qOrder.FieldByName('orID').AsString, qOrder.FieldByName('orDriver').AsInteger, qOrder.FieldByName('orTruck').AsInteger);

          RunTruckComp(0, True);
        finally
          slDupComp.Free;

          GridIndexE := 1;
          bConfirm.Visible := False;
          AdvPageControlMain.ActivePageIndex := 2;

          tmrGrdBlink.Enabled := True;
        end;
      end
      else
      begin
        ShowPanelMessage(tTypeError, ReadMessage(0));
      end;
    end;
  end;

  idcData.StopAnimation;
end;

procedure TfrmMain.ev_insert(EV_CODE: Integer; EV_IP, EV_DETAIL: string);
var
  s: string;
begin
  s := 'insert t_events (EV_CODE,EV_IP,EV_DETAIL) values (' + IntToStr(EV_CODE)
    + ',' + QuotedStr(EV_IP) + ',' + QuotedStr(EV_DETAIL) + ')';
  eSQL(s);
end;

procedure TfrmMain.Exit1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmMain.FormActivate(Sender: TObject);
var
  s, sReportURL, sDriverDup, sTruckDup, sTimeDup, sOrder: string;
  ErrorState: Boolean;
  BmStr: TBookmark;
  sStringAr: TStringArray;
  I: ShortInt;
begin
  if ReturnI then
  begin
    case AdvPageControlMain.ActivePageIndex of
      0:
        begin
          with qDriver do
          begin
            Close;
            SQL.Text := 'SELECT dvID, dvIDCard, dvDriveLicenseEXP, dvStatus, dvStatusTXT, dvName, dvNote, dvPict FROM vDriver WHERE dvID = ' + sDriverID;
            Open;

            if not Eof then
            begin
              DriverOrTruck := False;
            end;
          end;
        end;

      1:
        begin
          with qTruck do
          begin
            Close;
            SQL.Text := 'SELECT trID, trName, trCHKExpDate, trStatus, trTU, trName + '' - '' + Format(trTotalVolume, ''###,###'') + '' L.'' AS trNameSUM FROM vTruck WHERE trID = ' + sTruckID + '  and trMajorOil = 2';
            Open;

            if not Eof then
            begin
              DriverOrTruck := False;
              qTruckComp.Open;
            end;
          end;
        end;

      2:
        begin
          ErrorState := False;

          case SendTag of
            // DO
            0:
              begin
                sOrder := qOrder.FieldByName('orID').AsString;

                if Self.arValInput[frmInput.Tag] = EmptyStr then
                begin
                  ShowPanelMessage(tTypeError, ReadMessage(16));
                  Exit;
                end;

                with qFree do
                begin
                  Close;
                  SQL.Text :=
                    'SELECT odID, odDOC_NO FROM tOrderDetail WHERE odDOC_NO='
                    + QuotedStr(Self.arValInput[frmInput.Tag]) + ' AND odID<>' + sOrder;
                  Open;

                  if not Eof then
                  begin
                    //Get order information to Message
                    sStringAr := SelectReturnDataAr('SELECT dvName, trName, orCreated FROM vOrder WHERE orID = ' + sOrder, ['dvName', 'trName', 'orCreated'], 3);
                    sDriverDup := sStringAr[0];
                    sTruckDup := sStringAr[1];
                    sTimeDup := sStringAr[2];

                    s := 'update tOrder set orDO='''' where orID=' +
                      qOrder.FieldByName('orID').AsString + ';';
                    s := s + '';
                    s := s + ' update tOrderDetail set odDOC_NO='''' where odOrder='
                      + qOrder.FieldByName('orID').AsString + ' and odDOC_NO = ' + QuotedStr(Self.arValInput[frmInput.Tag]) + ';';
                    eSQL(s);

                    // LogEvent to SQL
                    ev_insert(9901, wksIP,
                      'Kiosk - Order [Duplicate OrderNumber] : ' + s);

//                    Self.arValInput[frmInput.Tag] := EmptyStr;
//                    GridIndexE := 1;

                    ShowPanelMessage(tTypeError, Format(ReadMessage(17),
                        [Self.arValInput[frmInput.Tag], sLineBreak, sTimeDup + sLineBreak, sDriverDup, sTruckDup + sLineBreak]));

                    //Re Add Order
                    bAddOrderClick2.Click;
                    Exit;
                  end;
                end;

                ReloadAllOrderQ;

//                s := 'update tOrderDetail set odDOC_NO=' +
//                  QuotedStr(Self.arValInput[frmInput.Tag]) + ' where odID=' +
//                  qOrderDetailHeader.FieldByName('odID').AsString + ';';
                s := 'update tOrderDetail set odDOC_NO=' +
                  QuotedStr(Self.arValInput[frmInput.Tag]) + ' where odOrder=' +
                  qOrder.FieldByName('orID').AsString + ' AND (odDOC_NO IS NULL OR odDOC_NO = '''') ;';

                // LogEvent to SQL
                ev_insert(9902, wksIP,
                  'Kiosk - Order [Update DO Header] : ' + s);
                eSQL(s);

                if grdOrderHeader.Row = 1 then
                begin
                  s := 'update tOrder set orDO=' +
                    QuotedStr(Self.arValInput[frmInput.Tag]) + ' where orID='
                    + qOrder.FieldByName('orID').AsString + '';
                  // LogEvent to SQL
                  ev_insert(9903, wksIP,
                    'Kiosk - Order [First Rec Update DO Main] : ' + s);
                  eSQL(s);

                  //
                  {
                  s := 'update tOrderComp set ocDOC_NO=' +
                  QuotedStr(Self.arValInput[frmInput.Tag]) + ' where ocOrder='
                  + qOrder.FieldByName('orID').AsString + '';
                  // LogEvent to SQL
                  ev_insert(9904, wksIP,
                    'Kiosk - Order [Update DO to Comp] : ' + s);
                  eSQL(s);
                  }
                end;

//                s := 'update tOrderDetail set odDOC_NO=' +
//                  QuotedStr(Self.arValInput[frmInput.Tag]) + ' where odOrder='
//                  + qOrderDetailHeader.FieldByName('ID').AsString + '';
//                // LogEvent to SQL
//                ev_insert(9904, wksIP,
//                  'Kiosk - Order [Update DO to Detail] : ' + s);
//                eSQL(s);

                ReloadAllOrderQ;
                ProgressOrder(0);

                if qOrderDetailHeader.Locate('odDOC_NO', Self.arValInput[frmInput.Tag], []) then
                  if not (qOrderDetailHeader.FieldByName('odCustomer').AsInteger > 0) then
                  begin
                    frmInput := TfrmInput.Create(Self);
                    frmInput.Tag := 1;
                    SendTag := 1;
                    frmInput.Show;
                  end;
              end;
            // CustomerCode
            1:
              begin
                // frmMain.ShowPanelMessage(tTypeMessage, 'CustCode ' + Self.arValInput[frmInput.Tag]);
                if Self.arValInput[frmInput.Tag] = EmptyStr then
                begin
                  ShowPanelMessage(tTypeError, ReadMessage(27));
                  Exit;
                end;

                s := 'update tOrder set orCustomer = ';
                s := s + '(SELECT top (1) cuID FROM mCustomer WHERE cuMJO = 2';
                s := s + ' and cuCodeEN = ' + QuotedStr(Self.arValInput[frmInput.Tag]) +') where orID = ' + qOrder.FieldByName('orID').AsString;

                // LogEvent to SQL
                ev_insert(9911, wksIP,
                  'Kiosk - OrderHeader [Update Customer to Main] : '
                  + s);
                eSQL(s);

//                qOrderDetailHeader.Refresh;
//
//                case qOrderDetailHeader.FieldByName('CUST_TYPE').AsInteger of
//                  0, 2:
//                    begin
//                      s := 'update tOrderDetail set odTruckType = 0 where odDOC_NO='
//                        + qOrderDetailHeader.FieldByName('DO_NO')
//                        .AsString.QuotedString + '';
//                    end;
//                  1:
//                    begin
//                      s := 'update tOrderDetail set odTruckType = 1 where odDOC_NO='
//                        + qOrderDetailHeader.FieldByName('DO_NO')
//                        .AsString.QuotedString + '';
//                    end;
//                end;

                s := 'update tOrderDetail set odCustomer = ';
                s := s + '(SELECT top (1) cuID FROM mCustomer WHERE cuMJO = 2';
                s := s + ' and cuCodeEN = ' + QuotedStr(Self.arValInput[frmInput.Tag]) +'), ';
                s := s + ' odTruckType = ';
                s := s + '(SELECT top (1) cuIsJobber FROM mCustomer WHERE cuMJO = 2';
                s := s + ' and cuCodeEN = ' + QuotedStr(Self.arValInput[frmInput.Tag]) +') ';
                s := s + ' where odDOC_NO = ' + QuotedStr(qOrderDetailHeader.FieldByName('odDOC_NO').AsString);

                // LogEvent to SQL
                ev_insert(9912, wksIP,
                  'Kiosk - OrderDetail [Update TruckType to OrderDetail] : ' +
                  s);
                eSQL(s);


                ReloadAllOrderQ;
//                qOrderDetailHeader.Refresh;
                ProgressOrder(0);

                qOrderDetailHeader.Locate('odDOC_NO', Self.arValInput[0], []);

                if grdOrderHeader.Row = 1 then
                  CompSEQ := 1
                else
                begin
                  if qOrderComp.Locate('ocPreset', 0, []) then
                    CompSEQ := qOrderComp.FieldByName('ocComp').AsInteger;
                end;

                with qOrderDetail do
                begin
                  while FieldByName('psCodeSale').AsString <> EmptyStr do
                  begin
                    Next;
                  end;
                end;

                //Start OrderDetailGrid Blink
                tmrGrdBlink.Enabled := True;

                // Retail Product
                if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 0 then
                begin
                  SendTag := 2;
                  cpProduct0.Visible := True;
                  gpnlProduct0.Visible := True;

                  cpProduct1.Visible := False;
                end;

                // Jobber Product
                if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 1 then
                begin
                  SendTag := 3;
                  cpProduct1.Visible := True;
                  cpProduct1.Left := 5;

                  gpnlProduct1.Visible := True;
                  cpProduct0.Visible := False;
                end;
              end;
            2, 3: // OrderDetailProduct Jobber, Retail
              begin
//                 frmMain.ShowPanelMessage(tTypeMessage, 'OrderDetailProduct Jobber, Retail ' + Self.arValInput[SendTag]);
                try
                  if Self.arValInput[SendTag] = EmptyStr then
                  begin
                    ShowPanelMessage(tTypeError, ReadMessage(28));
                    Exit;
                  end;

                  if qOrderDetail.FieldByName('odID').AsString = EmptyStr then
                  begin
                    ShowPanelMessage(tTypeError, ReadMessage(29));
                    Exit;
                  end;

                  with qFree do
                  begin
                    Close;
                    SQL.Text :=
                      'SELECT odSaleProduct FROM tOrderDetail WHERE (odSaleProduct=(select psID from mProductSale where pdMjOil = 2 and psCodeSale = '
                      + QuotedStr(Self.arValInput[SendTag]) +
                      ')) AND (odDOC_NO=' + QuotedStr(qOrderDetailHeader.FieldByName('odDOC_NO')
                      .AsString) + ')';
                    Open;

                    if not Eof then
                    begin
                      ShowPanelMessage(tTypeError, ReadMessage(30));
                      s := 'update tOrderDetail set odSaleProduct=0, odQTY=0 where odID='
                        + qOrderDetail.FieldByName('odID').AsString + '';
                      // LogEvent to SQL
                      ev_insert(9921, wksIP,
                        'Kiosk - OrderDetail [Update Clear Product to OrderDetail] : '
                        + s);

                      ErrorState := True;
                    end
                    else
                    begin
                      s := 'update tOrderDetail set odSaleProduct=(select psID from mProductSale where pdMjOil = 2 and psCodeSale = '
                        + QuotedStr(Self.arValInput[SendTag]) +
                        ') where odID=' + qOrderDetail.FieldByName('odID')
                        .AsString + '';
                      // LogEvent to SQL
                      ev_insert(9922, wksIP,
                        'Kiosk - OrderDetail [Update Product to OrderDetail] : '
                        + s);
                    end;

                    eSQL(s);
                  end;

                  qOrderDetail.DisableControls;

                  BmStr := qOrderDetail.Bookmark;
                  try
                     qOrderDetail.Refresh;
                  finally
                     qOrderDetail.Bookmark := BmStr;
                  end;

                  qOrderDetail.EnableControls;

                  ODetailSEQ := qOrderDetail.FieldByName('odSEQ').AsInteger;
                  ODetailProName := qOrderDetail.FieldByName('psDesc').AsString;

                  // LogEvent to SQL
                  ev_insert(9923, wksIP,
                  'Kiosk - [After Update OrderDetail] : DetailSEQ - ' + ODetailSEQ.ToString + ' DetailProName - ' + ODetailProName);

                  ProgressOrder(0);

                  if (not ErrorState) and (qOrderDetail.FieldByName('odQTY').AsInteger = 0) then
                  begin
                    frmInput := TfrmInput.Create(Self);
                    frmInput.Tag := 4;
                    SendTag := 4;
                    frmInput.Show;
                  end;

                except
                  on E: Exception do
                  begin
                    ev_insert(9999, wksIP, '3 : Error ' + E.Message);
                  end;
                end;
              end;
            4: // OrderDetailPreset
              begin
                try
                  if Self.arValInput[frmInput.Tag] = EmptyStr then
                  begin
                    ShowPanelMessage(tTypeError, ReadMessage(31));
                    Exit;
                  end;

                  if qOrderDetail.FieldByName('odID').AsString = EmptyStr then
                  begin
                    ShowPanelMessage(tTypeError, ReadMessage(32));
                    Exit;
                  end;

                  s := 'update tOrderDetail set odQTY=' +
                    VarToStrDef(Self.arValInput[frmInput.Tag], '0') +
                    ' where odID=' + qOrderDetail.FieldByName('odID')
                    .AsString;
                  // LogEvent to SQL
                  ev_insert(9941, wksIP,
                    'Kiosk - OrderDetail [Update QTY to OrderDetail] : ' + s);
                  eSQL(s);

                  //
                  {
                  if Self.arValInput[frmInput.Tag] <> '0' then
                  begin
                    s := 'UPDATE wsDeliveryOrderHeader SET TOTAL_WEIGHT=(SELECT SUM(tOrderKG) AS tTotalOrderKG FROM vOrderDetail WHERE (odOrder='
                      + qOrderDetailHeader.FieldByName('ID').AsString +
                      ') AND (tOrderKG IS NOT NULL)) WHERE ID=' +
                      qOrderDetailHeader.FieldByName('ID').AsString + '';
                    // LogEvent to SQL
                    ev_insert(9942, wksIP,
                      'Kiosk - OrderHeader [Update WEIGHT to OrderHeader] : ' +
                      s);
                    eSQL(s);

                    s := 'UPDATE wsDeliveryOrderHeader SET VOLUME=(SELECT SUM(odQTY) AS odQTY FROM vOrderDetail WHERE (odOrder='
                      + qOrderDetailHeader.FieldByName('ID').AsString +
                      ') AND (odQTY IS NOT NULL)) WHERE ID=' +
                      qOrderDetailHeader.FieldByName('ID').AsString + '';
                    // LogEvent to SQL
                    ev_insert(9943, wksIP,
                      'Kiosk - OrderHeader [Update VOLUME to OrderHeader] : ' +
                      s);
                    eSQL(s);
                  end
                  else
                  begin
                    s := 'update tOrderDetail set odSaleProduct=0, odQTY=0 where odID='
                      + qOrderDetail.FieldByName('odID').AsString + '';
                    // LogEvent to SQL
                    ev_insert(9944, wksIP,
                      'Kiosk - OrderDetail [Update Clear Product to OrderDetail] : '
                      + s);
                    eSQL(s);

                    s := 'UPDATE wsDeliveryOrderHeader SET TOTAL_WEIGHT=(SELECT SUM(tOrderKG) AS tTotalOrderKG FROM vOrderDetail WHERE (odOrder='
                      + qOrderDetailHeader.FieldByName('ID').AsString +
                      ') AND (tOrderKG IS NOT NULL)) WHERE ID=' +
                      qOrderDetailHeader.FieldByName('ID').AsString + '';
                    // LogEvent to SQL
                    ev_insert(9945, wksIP,
                      'Kiosk - OrderHeader [Update WEIGHT to OrderHeader] : ' +
                      s);
                    eSQL(s);

                    s := 'UPDATE wsDeliveryOrderHeader SET VOLUME=(SELECT SUM(odQTY) AS odQTY FROM vOrderDetail WHERE (odOrder='
                      + qOrderDetailHeader.FieldByName('ID').AsString +
                      ') AND (odQTY IS NOT NULL)) WHERE ID=' +
                      qOrderDetailHeader.FieldByName('ID').AsString + '';
                    // LogEvent to SQL
                    ev_insert(9946, wksIP,
                      'Kiosk - OrderHeader [Update VOLUME to OrderHeader] : ' +
                      s);
                    eSQL(s);
                  end;

                  }

                  //Debug Divired by Zero
                  qOrder.Refresh;
                  qOrderDetailHeader.Refresh;
                  qOrderDetail.Refresh;
                  qOrderComp.Refresh;

                  // LogEvent to SQL
                    ev_insert(9947, wksIP,
                      'Kiosk - [After Reload All Query]');
                    eSQL(s);

                  with qOrderDetail do
                  begin
                    for I := 1 to qOrderDetail.RecordCount do
                    begin
                      if FieldByName('psCodeSale').AsString <> EmptyStr then
                        Next;
                    end;

                    {while (FieldByName('psCodeSale').AsString <> EmptyStr) and  do
                    begin
                      Next;
                    end;}
                  end;

                  tmrGrdBlink.Enabled := True;

                  // Retail Product
                  if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 0 then
                  begin
                    gpnlProduct0.Visible := True;
                    cpProduct0.Visible := True;

                    gpnlProduct1.Visible := False;
                    cpProduct1.Visible := False;
                    SendTag := 2;
                  end;

                  // Jobber Product
                  if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 1 then
                  begin
                    gpnlProduct1.Visible := True;
                    cpProduct1.Visible := True;
                    cpProduct1.Left := 5;

                    cpProduct0.Visible := False;
                    gpnlProduct0.Visible := False;
                    SendTag := 3;
                  end;

                  ProgressOrder(0);

                  // LogEvent to SQL
                    ev_insert(9948, wksIP,
                      'Kiosk - [After Reload Progress Order]');
                    eSQL(s);
                except
                  on E: Exception do
                  begin
                    ev_insert(9999, wksIP, '4 : Error ' + E.Message);
                  end;

                end;
              end;
            5:
              // OrderCompPreset
              begin
                // frmMain.ShowPanelMessage(tTypeMessage, 'OrderCompPreset ' + Self.arValInput[frmInput.Tag]);
                try
                  if Self.arValInput[frmInput.Tag] = EmptyStr then
                  begin
                    ShowPanelMessage(tTypeError, ReadMessage(33));
                    Exit;
                  end;

                  if Self.arValInput[frmInput.Tag] = qOrderDetail.FieldByName('odDOC_NO').AsString then
                  begin
                    ShowPanelMessage(tTypeError, ReadMessage(34));
                    Exit;
                  end;

                  if Self.arValInput[frmInput.Tag] <> 0 then
                  begin
                    s := 'update tOrderComp set ocPreset= ' + Self.arValInput
                      [frmInput.Tag] + ' , ocDOC_NO=' + qOrderDetail.FieldByName
                      ('odDOC_NO').AsString.QuotedString + ', ocProduct=' +
                      qOrderDetail.FieldByName('odSaleProduct').AsString +
                      '  where ocID=' + qOrderComp.FieldByName('ocID')
                      .AsString + '';
                    // LogEvent to SQL
                    ev_insert(9951, wksIP,
                      'Kiosk - OrderComp [Update Preset to OrderComp] : ' + s);
                    eSQL(s);
                  end
                  else
                  begin
                    s := 'update tOrderComp set ocPreset= 0 ' + ' , ocDOC_NO=' +
                      qOrderDetail.FieldByName('odDOC_NO').AsString.QuotedString
                      + ', ocProduct= 0 ' + '  where ocID=' +
                      qOrderComp.FieldByName('ocID').AsString + '';
                    // LogEvent to SQL
                    ev_insert(9952, wksIP,
                      'Kiosk - OrderHeader [Update Clear Preset to OrderComp] : '
                      + s);
                    eSQL(s);
                  end;
                finally
                  qOrder.Refresh;
                  qOrderDetailHeader.Refresh;
                  qOrderDetail.Refresh;
                  qOrderComp.Refresh;
                  ProgressOrder(0);
                end;
              end;
            6: //Seal
              begin
                try
                  if Self.arValInput[frmInput.Tag] = EmptyStr then
                  begin
                    ShowPanelMessage(tTypeError, ReadMessage(35));
                    Exit;
                  end;

                  s := 'Update tOrder SET orSeal3 = ' +
                    VarToStrDef(Self.arValInput[frmInput.Tag], '0') +
                    ' WHERE orID = ' + qOrder.FieldByName('orID').AsString;
                  // LogEvent to SQL
                  ev_insert(9961, wksIP,
                    'Kiosk - Order [Update Seal to Order] : ' + s);
                  eSQL(s);

                  ReloadAllOrderQ;

                  qOrderDetail.FilterSQL := 'odSaleProduct <> 0 and odQTY <> 0';
                  with qOrderDetailPrint do
                  begin
                    Close;
                    SQL.Text := 'SELECT ROW_NUMBER() OVER(ORDER BY odID ASC) AS Row, * FROM vOrderDetail Where odOrder = ' + qOrder.FieldByName('orID').AsString + ' and odSaleProduct <> 0 and odQTY <> 0 order by odOrder,odSEQ ';
                    Open;
                  end;

                  // Report
//                   {$IFDEF RELEASE}
                  sReportURL := ExtractFilePath(ParamStr(0)) +
                    'wwwroot\Report\LATKiosk.fr3';
                  frxReportLAT.LoadFromFile(sReportURL);
                  frxReportLAT.PrepareReport(True);
                  frxReportLAT.PrintOptions.ShowDialog := False;
                  frxReportLAT.Print;
//                   {$ENDIF}

                  s := 'update tOrder SET orStatus=-99, orQSEQ = orID WHERE orID='
                    + qOrder.FieldByName('orID').AsString;
                  // LogEvent to SQL
                  ev_insert(9962, wksIP,
                    'Kiosk - Order [Update Status Done to Order] : ' + s);
                  eSQL(s);

                  ev_insert(-99, wksIP, 'KIOSK Print Order : ' +
                    qOrder.FieldByName('orID').AsString);

                  qOrder.Close;
                finally
                  ProgressOrder(0);
                end;
              end;
          end;

          if (SendTag < 6) then
            RunTruckComp(0);
        end;
    end;

    ReturnI := False;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
  function ProcessCount(const ExeName: String): Integer;
  var
    ContinueLoop: BOOL;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
  begin
    FSnapshotHandle:= CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    FProcessEntry32.dwSize:= SizeOf(FProcessEntry32);
    ContinueLoop:= Process32First(FSnapshotHandle, FProcessEntry32);
    Result:= 0;
    while Integer(ContinueLoop) <> 0 do begin
      if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
        UpperCase(ExeName)) or (UpperCase(FProcessEntry32.szExeFile) =
        UpperCase(ExeName))) then Inc(Result);
      ContinueLoop:= Process32Next(FSnapshotHandle, FProcessEntry32);
    end;
    CloseHandle(FSnapshotHandle);
  end;

  procedure checkProcessRunning;
  begin
    if ProcessCount(ExtractFileName(Application.ExeName)) > 1 then
    begin
      MessageDlg('Kiosk application is RUNNING! TERMINATED... ' + #13#10 + 'Please open application again.', mtError, [mbOK], 0);

      RzLauncher1.FileName := ExtractFilePath(Application.ExeName) + 'clearTAS.bat';
      RzLauncher1.Execute;
    end
    else
    begin
      WindowState := wsNormal;
    end;
  end;
begin
  checkProcessRunning;

  RzPropertyStoreDB.Load;

  wksIP := ipwIPInfo1.AdapterIPAddress;
  wksName := ipwIPInfo1.LocalHost;

  //Load Message
  slMessage := TStringList.Create;
  ReadMessage(-1, True);

  {$IFDEF DEBUG}
    bTestforDebug.Caption := 'TestData';
  {$ENDIF}
  ProgressOrder(0);

  ReturnI := False;
end;

procedure TfrmMain.GetDriver(SSID: string);
begin
  with qDriver do
  begin
    Close;
    SQL.Text := 'SELECT dvID, dvIDCard, dvDriveLicenseEXP, dvStatus, dvStatusTXT, dvName, dvNote, dvPict FROM vDriver WHERE dvMJO = 2 and dvIDCard LIKE ''%' + SSID + '''';
    Open;

    if Eof then
    begin
      Close;
      DBAdvPictureDriverPict.Refresh;

      ShowPanelMessage(tTypeError, ReadMessage(24));
    end
    else
    begin
      if RecordCount > 1 then
      begin
        Close;
        ShowPanelMessage(tTypeError, ReadMessage(36));
        ListOpening := True;

        frmListData := TfrmListData.Create(Self);
        DriverIDCard := SSID;
        frmListData.Tag := 0;
        frmListData.Show;
      end
      else
      begin
        if (FieldByName('dvDriveLicenseEXP').AsInteger >= FormatDatetime('YYYYMMDD', Date).ToInteger) and (FieldByName('dvStatus').AsInteger in [0, 1]) then
        begin
          if FieldByName('dvStatus').AsInteger = 1 then
          begin
            ShowPanelMessage(tTypeWaring, Format(ReadMessage(37), [FieldByName('dvStatusTXT').AsString, FieldByName('dvName').AsString, FieldByName('dvNote').AsString]));
          end;

          sDriverID := qDriver.FieldByName('dvID').AsString;
          DriverIDCard := qDriver.FieldByName('dvIDCard').AsString;

          DriverOrTruck := False;
        end
        else if not (FieldByName('dvStatus').AsInteger in [0, 1]) then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(38));

          DriverOrTruck := False;
          qDriver.Close;
          eSSID.Text := EmptyStr;
          idcData.StopAnimation;
          Exit;
        end
        else
        begin
          ShowPanelMessage(tTypeError, ReadMessage(39));

          DriverOrTruck := False;
          qDriver.Close;
          eSSID.Text := EmptyStr;
          idcData.StopAnimation;
          Exit;
        end;
      end;

      //Normally
      DriverOrTruck := True;
    end;
  end;
end;

function TfrmMain.GetEOFSelect(CommandSQL: string): Boolean;
begin
  Result := True;

  with qFree do
  begin
    Close;
    SQL.Text := CommandSQL;
    Open;

    if not Eof then
      Result := False;
  end;
end;

procedure TfrmMain.GetTruck(TruckLicense: string);
begin
  with qTruck do
  begin
    Close;
    SQL.Text := 'SELECT trID, trName, trCHKExpDate, trStatus, trTU, trName + '' - '' + Format(trTotalVolume, ''###,###'') + '' L.'' AS trNameSUM FROM vTruck WHERE trMajorOil = 2 and (NumTr = ' + QuotedStr(TruckLicense) + ' OR trName LIKE ''' + TruckLicense + '%'')';
    Open;

    if not qTruck.Eof then
    begin
      if RecordCount > 1 then
      begin
        Close;
        ShowPanelMessage(tTypeError, ReadMessage(3));
        ListOpening := True;

        frmListData := TfrmListData.Create(Self);
        sTruckPlate := TruckLicense;
        frmListData.Tag := 1;
        frmListData.Show;
      end
      else
      begin
        if (FieldByName('trCHKExpDate').AsInteger >= FormatDatetime('YYYYMMDD', Date).ToInteger) and (FieldByName('trStatus').AsInteger in [0, 1]) then
        begin
          sTruckID := qTruck.FieldByName('trID').AsString;
          sTruckPlate := qTruck.FieldByName('trName').AsString;

          DriverOrTruck := False;
          qTruckComp.Open;
        end
        else if not (FieldByName('trStatus').AsInteger in [0, 1]) then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(3));

          DriverOrTruck := False;
          sTruckID := EmptyStr;
          qTruck.Close;
          qTruckComp.Close;
          eTruck.Text := EmptyStr;
          idcData.StopAnimation;
          Exit;
        end
        else
        begin
          ShowPanelMessage(tTypeError, ReadMessage(4));

          DriverOrTruck := True;
          sTruckID := EmptyStr;
          qTruck.Close;
          qTruckComp.Close;
          eTruck.Text := EmptyStr;
          idcData.StopAnimation;
          Exit;
        end;
      end;
    end
    else
    begin
      DriverOrTruck := False;
      ShowPanelMessage(tTypeError, Format(ReadMessage(5), [TruckLicense]));

      sTruckID := EmptyStr;
      idcData.StopAnimation;
      Exit;
    end;

    //Normally
    DriverOrTruck := True;
  end;
end;

procedure TfrmMain.grdOrderHeaderClickCell(Sender: TObject;
  ARow, ACol: Integer);
begin
  SetExceptionMask(GetExceptionMask - []);

  if not(Sender as TDBAdvGrid).DataSource.DataSet.Active then
    Exit;
  //
  SealOrGrid := False;
  // FieldNameUpdate := (Sender as TDBAdvGrid).SelectedField.FieldName;
  GridIndexE := (Sender as TDBAdvGrid).Tag;

  case (Sender as TDBAdvGrid).Tag of
    1:
      begin
        GridIndexE := 1;

        case ACol of
          1:
            begin
              if qOrderDetailHeader.FieldByName('odDOC_NO').AsString <> EmptyStr then
              begin
                bUpdate.Visible := True;

                SendTag := 0;
              end
              else
              begin
                frmInput := TfrmInput.Create(Self);
                // frmInput.eInput.Text := qOrderHeader.FieldByName('DO_NO').AsString;
                // frmInput.Left := 115;
                // frmInput.Top := 304;
                // frmInput.Left := cpMain.Left + cpBG.Left + 3;
                // frmInput.Top := cpMain.Top + cpMain.Margins.Top + cpBG.Top + 11;
                // PWidth, PHeight
                frmInput.Tag := 0;
                SendTag := 0;
                frmInput.Show;
              end;
            end;
          2:
            begin
              if qOrderDetailHeader.FieldByName('odDOC_NO').AsString = EmptyStr then
              begin
                ShowPanelMessage(tTypeError, ReadMessage(40));

                Exit;
              end;

              if qOrderDetail.FieldByName('odCustomer').AsInteger > 0 then
              begin
                bUpdate.Visible := True;

                SendTag := 1;
              end
              else
              begin
                frmInput := TfrmInput.Create(Self);
                frmInput.Left := 115;
                frmInput.Top := 304;
                frmInput.Tag := 1;
                SendTag := 1;
                frmInput.Show;
              end;

              cpProduct0.Visible := False;
              cpProduct1.Visible := False;
            end;
        end;
      end;
    2:
      begin
        try
          mmoLog.Lines.Insert(0, 'GridSelect');

          GridIndexE := 2;

          if qOrderDetailHeader.FieldByName('odDOC_NO').AsString = EmptyStr then
          begin
            ShowPanelMessage(tTypeError, ReadMessage(40));
            Exit;
          end;

          if qOrderDetailHeader.FieldByName('odCustomer').AsInteger = 0 then
          begin
            ShowPanelMessage(tTypeError, ReadMessage(41));
            Exit;
          end;

          ODetailSEQ := qOrderDetail.FieldByName('odSEQ').AsInteger;
          ODetailProName := qOrderDetail.FieldByName(grdOrderDetail.Columns[3].FieldName).AsString;

          case ACol of
            2, 3:
              begin
                if qOrderDetail.FieldByName('psCodeSale').AsString <> EmptyStr then
                begin
                // Retail Product
                  if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 0 then
                  begin
                    gpnlProduct0.Visible := True;
                    cpProduct0.Visible := False;

                    gpnlProduct1.Visible := False;
                    cpProduct1.Visible := False;
                    SendTag := 2;
                  end;

                // Jobber Product
                  if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 1 then
                  begin
                    gpnlProduct1.Visible := True;
                    cpProduct1.Visible := False;
                    cpProduct1.Left := 5;

                    cpProduct0.Visible := False;
                    gpnlProduct0.Visible := False;
                    SendTag := 3;
                  end;

//                  grdOrderDetail.SelectCells(3, 1, 3, 1);
//                  qOrderDetail.First;
                end
                else
                begin
                // Retail Product
                  if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 0 then
                  begin
                    SendTag := 2;
                    cpProduct0.Visible := True;
                    gpnlProduct0.Visible := True;

                    cpProduct1.Visible := False;
                  end;

                // Jobber Product
                  if qOrderDetailHeader.FieldByName('odTruckType').AsInteger = 1 then
                  begin
                    SendTag := 3;
                    cpProduct1.Visible := True;
                    gpnlProduct1.Visible := True;
                    cpProduct1.Left := 5;

                    cpProduct0.Visible := False;
                  end;
                end;
              end;
          4:
            begin
              if qOrderDetail.FieldByName('odSaleProduct').AsInteger = 0 then
              begin
                ShowPanelMessage(tTypeError, ReadMessage(7));
                Exit;
              end;

              if qOrderDetail.FieldByName('odQTY').AsInteger <> 0 then
              begin
                SendTag := 4;
                cpProduct0.Visible := False;
                cpProduct1.Visible := False;
              end
              else
              begin
                frmInput := TfrmInput.Create(Self);
                frmInput.Tag := 4;
                SendTag := 4;
                frmInput.Show;
              end;
            end;
        end;

        //Except on Select Order Detail Grid
        except
          on E: Exception do
          begin
            ev_insert(9999, wksIP, '2 : Error ' + E.Message);
          end;
        end;
      end;
    3:
      begin
        if not qOrderComp.Active then
          Exit;

        CompSEQ := qOrderComp.RecNo;
        GridIndexE := 3;

        //
        {
          qOrderHeader.Locate('DO_NO', qOrderComp.FieldByName('ocDOC_NO').AsString, []);

          frmInput := TfrmInput.Create(Self);

          //Retail Product
          if qOrderHeader.FieldByName('CUST_TYPE').AsInteger = 0 then
          begin
          frmInput.Tag := 2;
          end;

          //Jobber Product
          if qOrderHeader.FieldByName('CUST_TYPE').AsInteger = 1 then
          begin
          frmInput.Tag := 3;
          end;

          frmInput.Show;

        }

        if ACol = 2 then
          Exit;

        if qOrderDetail.FieldByName('odSaleProduct').AsInteger = 0 then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(7));
          Exit;
        end;

        if qOrderDetail.FieldByName('odQTY').AsInteger = 0 then
        begin
          ShowPanelMessage(tTypeError, ReadMessage(8));
          Exit;
        end;

        frmInput := TfrmInput.Create(Self);

        with frmInput do
        begin
          Tag := 5;
          SendTag := 5;

          with qFree do
          begin
            Close;
            SQL.Text := 'SELECT tcVolume1, tcVolume2, tcVolume3, tcVolume4 FROM vTruckComp WHERE tcTUNO = ' +
              qOrder.FieldByName('trTU').AsString + ' AND tcCompID = ' +
              qOrderComp.FieldByName('ocComp').AsString;
            Open;

            if qFree.FieldValues['tcVolume1'] > 0 then
            begin
              bCompPreset1.Caption := qFree.FieldByName('tcVolume1').AsString;
              bCompPreset1.Tag := qFree.FieldByName('tcVolume1').AsInteger;
              bCompPreset1.Visible := True;
            end;
            if qFree.FieldValues['tcVolume2'] > 0 then
            begin
              bCompPreset2.Caption := qFree.FieldByName('tcVolume2').AsString;
              bCompPreset2.Tag := qFree.FieldByName('tcVolume2').AsInteger;
              bCompPreset2.Visible := True;
            end;
            if qFree.FieldValues['tcVolume3'] > 0 then
            begin
              bCompPreset3.Caption := qFree.FieldByName('tcVolume3').AsString;
              bCompPreset3.Tag := qFree.FieldByName('tcVolume3').AsInteger;
              bCompPreset3.Visible := True;
            end;
            if qFree.FieldValues['tcVolume4'] > 0 then
            begin
              bCompPreset4.Caption := qFree.FieldByName('tcVolume4').AsString;
              bCompPreset4.Tag := qFree.FieldByName('tcVolume4').AsInteger;
              bCompPreset4.Visible := True;
            end;

            qFree.Close;
          end;

          Show;
        end;
      end;
  end;
end;

function TfrmMain.IsFirstRecord(ADataSet: TDataSet): Boolean;
begin
  Result := not ADataSet.IsEmpty;
  if not Result then
    Exit;
  Result := ADataSet.Bof;
  // if ADataSet is already at BOF there is no point to continue
  if not Result then
  begin
    ADataSet.DisableControls;
    try
      // BmStr := ADataSet.Bookmark;
      try
        ADataSet.Prior;
        Result := ADataSet.Bof;
      finally
        ADataSet.Next;
        // ADataSet.Bookmark := BmStr;
      end;
    finally
      ADataSet.EnableControls;
    end;
  end;
end;

procedure TfrmMain.ProgressOrder(SEQ: Smallint);
var
  s: string;
  i: ShortInt;
begin
  if not qOrder.Active then
  begin
    qOrder.Close;
    qOrderDetail.Close;
    qOrderDetail.FilterSQL := EmptyStr;
    qOrderComp.Close;
    qOrderDetailHeader.Close;
    qTruckComp.Close;
    qDriver.Close;
    qCustomer.Close;
    qTruck.Close;
    qProductSale.Close;
    qOrderDetailPrint.Close;

    for i := 0 to 6 do
    begin
      arValInput[i] := EmptyStr;
    end;

    if InputOpening then
      frmInput.Release;

    if ListOpening then
      frmListData.Release;

    bConfirm.Visible := True;
    eSealNumber.Text := EmptyStr;
    DriverOrTruck := True;
    AdvPageControlMain.ActivePageIndex := 0;
    pgOrder.Position := 0;
    sDriverID := EmptyStr;
    eSSID.Text := EmptyStr;
    eTruck.Text := EmptyStr;
    eCard.Text := EmptyStr;

    //Reset to use SSID
    bCreateTicketByCard.Caption := ReadMessage(21);
    bCreateTicketByCard.Font.Size := 22;
    tCaptionFirst.Caption := ReadMessage(22);
    tCaptionFirst2.Caption := ReadMessage(23);

    eCard.Visible := False;
    eSSID.Visible := True;

    Exit;
  end;

  if qOrder.FieldByName('orDriver').AsInteger > 0 then
  begin
    pgOrder.Position := 20;
  end;

  if qOrder.FieldByName('orTruck').AsInteger > 0 then
  begin
    pgOrder.Position := 30;
  end;

  if qOrder.FieldByName('orDO').AsString <> EmptyStr then
  begin
    pgOrder.Position := 40;
  end;

  if qOrder.FieldByName('orCustomer').AsInteger > 0 then
  begin
    pgOrder.Position := 50;
  end;


  // CheckHeader
//  with qFree do
//  begin
//    Close;
//    s := 'SELECT * FROM wsDeliveryOrderHeader WHERE (DO_NO<>' +
//      EmptyStr.QuotedString + ') AND (SHIP_TO<>' + EmptyStr.QuotedString +
//      ') AND (ORDER_MAIN=' + qOrder.FieldByName('orID').AsString + ')';
//    SQL.Text := s;
//    Open;
//    if not Eof then
//      pgOrder.Position := 50;
//  end;


  if (qOrder.FieldByName('orDriver').AsInteger > 0) AND
    (qOrder.FieldByName('orTruck').AsInteger > 0) then
    // CheckDetail
    with qFree do
    begin
      Close;
      s := 'SELECT odID FROM tOrderDetail WHERE (odDOC_NO<>' +
        EmptyStr.QuotedString + ') AND (odQTY>0) AND (odOrder=' +
        qOrder.FieldByName('orID').AsString + ')';
      SQL.Text := s;
      Open;
      if not Eof then
        pgOrder.Position := 70;
    end;

  // CheckComp
  with qFree do
  begin
    Close;
    s := 'SELECT ocID FROM tOrderComp WHERE (ocDOC_NO<>' + EmptyStr.QuotedString +
      ') AND (ocPreset>0) AND (ocOrder=' + qOrder.FieldByName('orID')
      .AsString + ')';
    SQL.Text := s;
    Open;

    if (not Eof) AND (chkOrderCompNonN) then
    begin
      bConfirm.Visible := True;
      pgOrder.Position := 100;
    end
    else
      bConfirm.Visible := False;
  end;
end;

procedure TfrmMain.qOrderAfterFetch(DataSet: TCustomDADataSet);
begin
  idcData.StopAnimation;
end;

procedure TfrmMain.qOrderBeforeFetch(DataSet: TCustomDADataSet;
  var Cancel: Boolean);
begin
  idcData.StartAnimation;
  Application.ProcessMessages;
end;

function TfrmMain.ReadMessage(Index: SmallInt; const LoadMessage: Boolean): string;
var
  i: Integer;
begin
  if LoadMessage then
  begin
    slMessage.LoadFromFile(ExtractFileDir(Application.ExeName) + '/KioskMessage.txt', TEncoding.UTF8);

    for i := 0 to slMessage.Count - 1 do
    begin
      slMessage[i] := slMessage[i];
    end;

    Result := 'Loaded';
  end
  else
  begin
    Result := slMessage[Index];
  end;
end;

procedure TfrmMain.ReloadAllOrderQ;
begin
  if qOrder.Active then
    qOrder.Refresh;

  if qOrderDetailHeader.Active then
    qOrderDetailHeader.Refresh;

  if qOrderDetail.Active then
    qOrderDetail.Refresh;

  if qOrderComp.Active then
    qOrderComp.Refresh;
end;

procedure TfrmMain.RunTruckComp(CompPanel: ShortInt;
  const ProductAssign: Boolean = False; const CompDup: Boolean = False);
var
  I, J: ShortInt;
  orIDRef, BCPCapt: string;
  CompVolume: SmallInt;
begin
  qOrderComp.Refresh;
  qOrderComp.First;

  if qOrderComp.Eof then
  begin
    // LogEvent to SQL
    ev_insert(9999, wksIP,
                        'Kiosk - Order [not GenTruckComp] : Order Compartment = 0 - orID = ' + qOrder.FieldByName('orID').AsString + ' | qOrderCompCount = ' + qOrderComp.RecordCount.ToString);

    ShowPanelMessage(tTypeError, ReadMessage(42));
    Exit;
  end;

  cpComp1.Visible := False;
  cpComp2.Visible := False;
  cpComp3.Visible := False;
  cpComp4.Visible := False;
  cpComp5.Visible := False;
  cpComp6.Visible := False;
  cpComp7.Visible := False;
  cpComp8.Visible := False;
  cpComp9.Visible := False;
  cpComp10.Visible := False;

  // cpBGComp.Width := 86 + (70 * qOrderComp.RecordCount);

  for I := 1 to qOrderComp.RecordCount do
  begin
    if TCurvyPanel(Self.FindComponent('cpComp' + I.ToString)) <> nil then
    begin
      with TCurvyPanel(Self.FindComponent('cpComp' + I.ToString)) do
      begin
        try
          if I = 1 then
          begin
            if qOrderComp.RecordCount < 8 then
              Left := 190
            else
              Left := 86
          end
          else
            Left := TCurvyPanel(Self.FindComponent('cpComp' + (I - 1).ToString))
              .Left + TCurvyPanel
              (Self.FindComponent('cpComp' + (I - 1).ToString)).Width + 3;

          if qOrderComp.RecordCount < 8 then
            Width := (550 div qOrderComp.RecordCount)
          else
            Width := (620 div qOrderComp.RecordCount);

          // mmoComp.Lines.Add(TCurvyPanel(Self.FindComponent('cpComp' + (I-1).ToString)).Left.ToString);

          Visible := True;

          for J := 1 to 4 do
          begin
            if qOrderComp.FieldByName('tcVolume' + J.ToString).AsInteger <> 0
            then
            begin
              //Check in Another MJO
              orIDRef := qOrder.FieldByName('orIDRef').AsString;

              //Get Data from Another MJO
              if orIDRef <> '-1' then
              begin
                BCPCapt := SelectReturnData('SELECT psDesc FROM vOrderComp WHERE ocOrder = ' + orIDRef + ' and ocComp = ' + I.ToString, 'psDesc');
                CompVolume := StrToIntDef(SelectReturnData('SELECT ocPreset FROM vOrderComp WHERE ocOrder = ' + orIDRef + ' and ocComp = ' + I.ToString, 'ocPreset'), 0);
              end
              else
              begin
                BCPCapt := EmptyStr;
                CompVolume := 0;
              end;

              if TLabel(Self.FindComponent('tVolume' + I.ToString + J.ToString))
                <> nil then
              begin
                with TLabel(Self.FindComponent('tVolume' + I.ToString +
                  J.ToString)) do
                begin
                  try
                    if BCPCapt = EmptyStr then
                    begin
                      Caption := FormatFloat('#,###',
                        qOrderComp.FieldByName('tcVolume' + J.ToString)
                        .AsInteger);
                    end
                    else
                    begin
                      with TLabel(Self.FindComponent('tVolumeProduct' +
                        I.ToString)) do
                      begin
                        Caption := '�ҧ�ҡ - ' + BCPCapt;
                        Height := 140;
                        Visible := True;
                      end;

                      with TLabel(Self.FindComponent('tVolumeProductPreset' +
                        I.ToString)) do
                      begin
                        Caption := FormatFloat('#,###',
                          CompVolume);
                        Visible := True;
                      end;
                    end;

                    if (qOrderComp.FieldByName('ocProduct').AsInteger > 0) and
                      (qOrderComp.FieldByName('ocPreset').AsInteger > 0) or
                      (BCPCapt <> EmptyStr) then
                      Visible := False
                    else
                      Visible := True;
                  except

                  end;
                end;
              end;
            end
            else
              TLabel(Self.FindComponent('tVolume' + I.ToString + J.ToString))
                .Visible := False
          end;

          if TLabel(Self.FindComponent('tCompTitle' + I.ToString)) <> nil then
          begin
            mmoComp.Lines.Add('tCompTitle' + I.ToString);

            with TLabel(Self.FindComponent('tCompTitle' + I.ToString)) do
            begin
              try
                if (qOrderComp.FieldByName('ocProduct').AsInteger > 0) and
                  (qOrderComp.FieldByName('ocPreset').AsInteger > 0) or
                  (BCPCapt <> EmptyStr) then
                begin
                  if BCPCapt = EmptyStr then
                  begin
                    Font.Color := clLime;

                    with TLabel(Self.FindComponent('tVolumeProduct' +
                      I.ToString)) do
                    begin
                      Caption := qOrderComp.FieldByName('ocDOC_NO').AsString +
                        ' - ' + qOrderComp.FieldByName('psDesc').AsString;
                      Height := 140;
                      Visible := True;
                    end;

                    with TLabel(Self.FindComponent('tVolumeProductPreset' +
                      I.ToString)) do
                    begin
                      Caption := FormatFloat('#,###',
                        qOrderComp.FieldByName('ocPreset').AsInteger);
                      Visible := True;
                    end;
                  end else
                  begin
                    Font.Color := clRed;
                  end;
                end
                else
                begin
                  Font.Color := clWhite;

                  with TLabel(Self.FindComponent('tVolumeProduct' +
                    I.ToString)) do
                  begin
                    Visible := False;
                  end;

                  with TLabel(Self.FindComponent('tVolumeProductPreset' +
                    I.ToString)) do
                  begin
                    Visible := False;
                  end;
                end;
              except

              end;
            end;
          end;

        except
          on E: Exception do
          begin
            ev_insert(9999, wksIP, '1 : Error ' + E.Message);
          end;
        end;
      end;
    end;
    qOrderComp.Next;
  end;

  qOrderComp.First;
end;

function TfrmMain.SelectReturnData(CommandText, FieldReturn: string): string;
begin
  with qFree do
  begin
    Close;
    SQL.Text := CommandText;
    Open;

    if not Eof then
    begin
      Result := FieldByName(FieldReturn).AsString;
    end
    else
    begin
      Result := EmptyStr;
    end;
  end;
end;

function TfrmMain.SelectReturnDataAr(CommandText :string;
  FieldReturn: TStringArray; const Length: ShortInt = 1): TStringArray;
var
  I: ShortInt;
begin
  SetLength(Result, Length);

  with qFree do
  begin
    Close;
    SQL.Text := CommandText;
    Open;

    for I := 0 to Length - 1 do
      if not Eof then
      begin
        Result[I] := FieldByName(FieldReturn[I]).AsString;
      end
      else
      begin
        Result[I] := EmptyStr;
      end;
  end;
end;

procedure TfrmMain.ShowPanelMessage(TitleType: TTitleType; Text: string);
begin
  frmMessage.BringToFront;

  case TitleType of
    tTypeWaring:
      begin
        with frmMessage.tMessageTitle do
        begin
          Caption := '����͹';
          Color := $0051A8FF;
          Font.Color := clWindowText;
          frmMessage.tMessageBody.Caption := Text;
          frmMessage.ShowModal;
        end;
      end;
    tTypeError:
      begin
        with frmMessage.tMessageTitle do
        begin
          ReturnI := False;
          Caption := '�Դ��Ҵ !';
          Color := $004242FF;
          Font.Color := clWhite;
          frmMessage.tMessageBody.Caption := Text;
          frmMessage.ShowModal;
        end;
      end;
    tTypeMessage:
      begin
        with frmMessage.tMessageTitle do
        begin
          Caption := '��ͤ���';
          Color := clGray;
          Font.Color := clWhite;
          frmMessage.tMessageBody.Caption := Text;
          frmMessage.ShowModal;
        end;
      end;
  end;
end;

procedure TfrmMain.tmrFadeTimer(Sender: TObject);
begin
  if FlagFade then
  begin
    AlphaBlendValue := (AlphaBlendValue + 5);
    //
    if AlphaBlendValue = 255 then
      FlagFade := False;
  end
  else
  begin

    AlphaBlendValue := (AlphaBlendValue - 5);

    if AlphaBlendValue = 0 then
      FlagFade := True;
  end;
end;

procedure TfrmMain.tmrGrdBlinkTimer(Sender: TObject);
begin
  if (not qOrderDetailHeader.Active) and (not qOrderDetail.Active) then
  Exit;

  try
    if (qOrderDetailHeader.FieldByName('odDOC_NO').AsString <> EmptyStr) and {(not qOrder.FieldByName('orCustomer').AsInteger > 0) and} (qOrderDetail.FieldByName('psCodeSale').AsString = EmptyStr) then
    begin
      grdOrderDetail.SelectCells(1, grdOrderDetail.Row, 1, grdOrderDetail.Row);
      grdOrderDetail.ScrollBy(0, 0);

      case grdOrderDetail.ActiveRowColor of
        clRed:
          grdOrderDetail.ActiveRowColor := clWhite;
      else
        grdOrderDetail.ActiveRowColor := clRed;
      end;
    end
    else
    begin
      grdOrderDetail.ActiveRowColor := clInfoBk;
      tmrGrdBlink.Enabled := False;
    end;
  except
    on E: Exception do
    begin
      ev_insert(9999, wksIP, '5 : Error ' + E.Message);
    end;

  end;
end;

procedure TfrmMain.tVolume11Click(Sender: TObject);
var
 Comp: ShortInt;
 trTU: string;
begin
  //
  {
    if not qOrderComp.Active then
    Exit;

    if qOrderComp.Locate('ocComp', (Sender as TLabel).Tag, []) then
    begin
    CompSEQ := qOrderComp.RecNo;
    GridIndexE := 3;

    qOrderHeader.Locate('DO_NO', qOrderComp.FieldByName('ocDOC_NO').AsString, []);

    frmInput := TfrmInput.Create(Self);

    //Retail Product
    if qOrderHeader.FieldByName('CUST_TYPE').AsInteger = 0 then
    begin
    frmInput.Tag := 2;
    end;

    //Jobber Product
    if qOrderHeader.FieldByName('CUST_TYPE').AsInteger = 1 then
    begin
    frmInput.Tag := 3;
    end;

    frmInput.Show;
    end;

  }

  if not qOrderComp.Active then
    Exit;

  //If bcp detected then exit dont assign
  if AnsiContainsStr(TLabel(Self.FindComponent('tVolumeProduct' + (Sender as TLabel).Tag.ToString)).Caption, '�ҧ�ҡ') then
  begin
    ShowPanelMessage(tTypeError, ReadMessage(6));
    Exit;
  end;

  if qOrderComp.Locate('ocComp', (Sender as TLabel).Tag, []) then
  begin
    CompSEQ := qOrderComp.RecNo;
    Comp := qOrderComp.FieldByName('ocComp').AsInteger;
    trTU := qOrder.FieldByName('trTU').AsString;
    // ODetailSEQ := qOrderDetail.FieldByName('odSEQ').AsInteger;

    ODetailProName := qOrderDetail.FieldByName
      (grdOrderDetail.Columns[3].FieldName).AsString;
    GridIndexE := 3;

    if qOrderDetail.FieldByName('odSaleProduct').AsInteger = 0 then
    begin
      ShowPanelMessage(tTypeError, ReadMessage(7));
      Exit;
    end;

    if qOrderDetail.FieldByName('odQTY').AsInteger = 0 then
    begin
      ShowPanelMessage(tTypeError, ReadMessage(8));
      Exit;
    end;

    if (Comp > 0) and (trTU <> EmptyStr) then
    begin
      frmInput := TfrmInput.Create(Self);

      with frmInput do
      begin
        Tag := 5;
        SendTag := 5;

        with qFree do
        begin
          Close;
          SQL.Text := 'SELECT tcVolume1, tcVolume2, tcVolume3, tcVolume4 FROM vTruckComp WHERE tcTUNO = ' +
            trTU + ' AND tcCompID = ' + IntToStr(Comp);
          Open;

          if qFree.FieldValues['tcVolume1'] > 0 then
          begin
            bCompPreset1.Caption := qFree.FieldByName('tcVolume1').AsString;
            bCompPreset1.Tag := qFree.FieldByName('tcVolume1').AsInteger;
            bCompPreset1.Visible := True;
          end;
          if qFree.FieldValues['tcVolume2'] > 0 then
          begin
            bCompPreset2.Caption := qFree.FieldByName('tcVolume2').AsString;
            bCompPreset2.Tag := qFree.FieldByName('tcVolume2').AsInteger;
            bCompPreset2.Visible := True;
          end;
          if qFree.FieldValues['tcVolume3'] > 0 then
          begin
            bCompPreset3.Caption := qFree.FieldByName('tcVolume3').AsString;
            bCompPreset3.Tag := qFree.FieldByName('tcVolume3').AsInteger;
            bCompPreset3.Visible := True;
          end;
          if qFree.FieldValues['tcVolume4'] > 0 then
          begin
            bCompPreset4.Caption := qFree.FieldByName('tcVolume4').AsString;
            bCompPreset4.Tag := qFree.FieldByName('tcVolume4').AsInteger;
            bCompPreset4.Visible := True;
          end;

          qFree.Close;
        end;

        Show;
      end;
    end;
  end;
end;

procedure TfrmMain.UniConnectionTASError(Sender: TObject; E: EDAError;
  var Fail: Boolean);
begin
  if Fail then
  begin
    if E.ClassName = 'TUniQuery' then
      ev_insert(9999, wksIP, 'Kiosk : Error ' + (E.Component as TUniQuery).Name + ' - ' + (E.Component as TUniQuery).SQL.Text + ' - ' + E.Message);

    if E.ClassName = 'TUniSQL' then
      ev_insert(9999, wksIP, 'Kiosk : Error ' + (E.Component as TUniSQL).Name + ' - ' + (E.Component as TUniSQL).SQL.Text + ' - ' + E.Message);
  end;
end;

procedure TfrmMain.UniSQLAfterExecute(Sender: TObject; Result: Boolean);
begin
  idcData.StopAnimation;

  // mmoLog.Text := UniSQL.SQL.Text;
end;

procedure TfrmMain.UniSQLBeforeExecute(Sender: TObject);
begin
  idcData.StartAnimation;
  Application.ProcessMessages;
end;

end.
