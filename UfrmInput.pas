unit UfrmInput;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AdvGlassButton, Vcl.StdCtrls,
  CurvyControls, Vcl.Imaging.pngimage, Vcl.ExtCtrls, se_image, Data.DB, MemDS,
  DBAccess, Uni;

type
  TfrmInput = class(TForm)
    cpInput: TCurvyPanel;
    tInputTitle: TLabel;
    eInput: TCurvyEdit;
    iLogo: TseImage;
    bInputOK: TAdvGlassButton;
    bInputBack: TAdvGlassButton;
    qFree: TUniQuery;
    cpProduct1: TCurvyPanel;
    bProduct01: TAdvGlassButton;
    bProduct02: TAdvGlassButton;
    bProduct03: TAdvGlassButton;
    bProduct04: TAdvGlassButton;
    bProduct05: TAdvGlassButton;
    bProduct06: TAdvGlassButton;
    bProduct07: TAdvGlassButton;
    cpConfirm: TCurvyPanel;
    bInputBack2: TAdvGlassButton;
    cpProduct0: TCurvyPanel;
    bProduct14: TAdvGlassButton;
    bProduct15: TAdvGlassButton;
    bProduct16: TAdvGlassButton;
    bProduct17: TAdvGlassButton;
    bInputBack3: TAdvGlassButton;
    cpCompPreset: TCurvyPanel;
    bCompPreset1: TAdvGlassButton;
    bCompPreset2: TAdvGlassButton;
    bCompPreset3: TAdvGlassButton;
    bCompPreset4: TAdvGlassButton;
    bCompPresetBack: TAdvGlassButton;
    bProduct13: TAdvGlassButton;
    bProduct12: TAdvGlassButton;
    bProduct11: TAdvGlassButton;
    bProduct1Pass: TAdvGlassButton;
    tCompTitle: TLabel;
    tProduct0Comp: TLabel;
    t1: TLabel;
    tProduct1Comp: TLabel;
    bProduct0Pass: TAdvGlassButton;
    bDeleteCompPreset: TAdvGlassButton;
    procedure eInputControlClick(Sender: TObject; Index: Integer);
    procedure bInputBackClick(Sender: TObject);
    procedure bInputOKClick(Sender: TObject);
    procedure eInputChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInput: TfrmInput;

implementation

uses
  UfrmMain;

{$R *.dfm}

procedure TfrmInput.bInputBackClick(Sender: TObject);
begin
  frmMain.ReturnI := False;;
  frmMain.InputOpening := False;
  Self.Release;
end;

procedure TfrmInput.bInputOKClick(Sender: TObject);
begin
  //Set Default
  frmMain.ReturnI := False;

  case Self.Tag of
    // DO
    0:
      begin
        if eInput.Text = EmptyStr then
        begin
          frmMain.ShowPanelMessage(tTypeError, frmMain.ReadMessage(43));
          Exit;
        end;

        frmMain.arValInput[Self.Tag] := eInput.Text;
        frmMain.ReturnI := True;
        frmMain.InputOpening := False;

        Self.Release;
      end;
    // CustomerCode
    1:
      begin
        if eInput.Text = EmptyStr then
        begin
          frmMain.ShowPanelMessage(tTypeError, frmMain.ReadMessage(44));
          Exit;
        end;

        with qFree do
        begin
          Close;
          SQL.Text :=
            'SELECT DISTINCT cuCodeEN, cuName, cuIsJobber FROM mCustomer where cuCodeEN = '''
            + eInput.Text + ''' and cuMJO = 2 order by cuCodeEN';
          Open;

          if not EOF then
          begin
            eInput.Text := FieldByName('cuCodeEN').AsString;

            if qFree.FieldByName('cuIsJobber').AsInteger = 1 then
            begin
              if qFree.FieldByName('cuName').AsString <> '-' then
                frmMain.ShowPanelMessage(tTypeMessage, Format(frmMain.ReadMessage(45), [FieldByName('cuName').AsString]))
              else if qFree.FieldByName('cuName').AsString <> '-' then
                frmMain.ShowPanelMessage(tTypeMessage, Format(frmMain.ReadMessage(45), [FieldByName('cuName').AsString]))
              else
                frmMain.ShowPanelMessage(tTypeMessage, frmMain.ReadMessage(46));
            end
            else
            begin
              if qFree.FieldByName('cuName').AsString <> '-' then
                frmMain.ShowPanelMessage(tTypeMessage, Format(frmMain.ReadMessage(47), [FieldByName('cuName').AsString]))
              else if qFree.FieldByName('cuName').AsString <> '-' then
                frmMain.ShowPanelMessage(tTypeMessage, Format(frmMain.ReadMessage(47), [FieldByName('cuName').AsString]))
              else
                frmMain.ShowPanelMessage(tTypeMessage, frmMain.ReadMessage(48));
            end;
          end
          else
          begin
            frmMain.ShowPanelMessage(tTypeError, frmMain.ReadMessage(49));
            Exit;
          end;
        end;

        frmMain.arValInput[Self.Tag] := eInput.Text;
        frmMain.ReturnI := True;
        frmMain.InputOpening := False;

        frmMain.OnActivate(Sender);
        Self.Release;
      end;
    2: // OrderDetailProduct Jobber
      begin
        frmMain.arValInput[Self.Tag] := (Sender as TAdvGlassButton)
          .Tag.ToString;
        frmMain.ReturnI := True;
        frmMain.InputOpening := False;

        Self.Release;
      end;
    3: // OrderDetailProduct Retail
      begin
        frmMain.arValInput[Self.Tag] := (Sender as TAdvGlassButton)
          .Tag.ToString;
        frmMain.ReturnI := True;
        frmMain.InputOpening := False;

        Self.Release;
      end;
    4: // OrderDetailPreset
      begin
        if eInput.Text = EmptyStr then
        begin
          frmMain.ShowPanelMessage(tTypeError, frmMain.ReadMessage(50));
          Exit;
        end;

        if Length(eInput.Text) <= 2 then
          frmMain.arValInput[Self.Tag] :=
            (StrToInt(eInput.Text) * 1000).ToString
        else
          frmMain.arValInput[Self.Tag] := eInput.Text;

        frmMain.ReturnI := True;
        frmMain.InputOpening := False;

        Self.Release;
      end;
    5: // OrderCompPreset
      begin
        frmMain.arValInput[Self.Tag] := (Sender as TAdvGlassButton)
          .Tag.ToString;
        frmMain.ReturnI := True;
        frmMain.InputOpening := False;

        Self.Release;
      end;
    6: // SealCount
      begin
        if (eInput.Text = EmptyStr) or (StrToInt(eInput.Text) <= 0) then
        begin
          frmMain.ShowPanelMessage(tTypeError, frmMain.ReadMessage(51));

          Exit;
        end;

        frmMain.arValInput[Self.Tag] := eInput.Text;
        frmMain.ReturnI := True;
        frmMain.InputOpening := False;

        Self.Release;
      end;
  end;
end;

procedure TfrmInput.eInputChange(Sender: TObject);
begin
  case Self.Tag of // DO
    0:
      begin

      end;
    // CustomerCode
    1:
      begin
        if Length(frmInput.eInput.Text) >= 7 then
        begin
          with qFree do
          begin
            Close;
            SQL.Text :=
              'SELECT DISTINCT cuCodeEN, cuName, cuIsJobber FROM mCustomer where cuCodeEN LIKE '''
              + eInput.Text +
              ''' and cuMJO = 2 and cuIsJobber in (0, 1) order by cuCodeEN';
            Open;

            if not EOF then
            begin
              eInput.Text := FieldByName('cuCodeEN').AsString;
            end
            else
            begin
              frmMain.ShowPanelMessage(tTypeError, frmMain.ReadMessage(49));
            end;
          end;
        end;
      end;
    2: // OrderDetailProduct
      begin
      end;
    3: // OrderDetailPreset
      begin
      end;
    4: // OrderCompPreset
      begin
      end;
  end;
end;

procedure TfrmInput.eInputControlClick(Sender: TObject; Index: Integer);
begin
  eInput.Text := EmptyStr;
end;

procedure TfrmInput.FormCreate(Sender: TObject);
begin
  // {$IFDEF DEBUG}
  // Self.Position := poScreenCenter;
  // Self.WindowState := wsNormal;
  // {$ENDIF}
end;

procedure TfrmInput.FormShow(Sender: TObject);
begin
  // eInput.Text := EmptyStr;
  frmMain.InputOpening := True;

  Self.Top := 275;
//  Self.Left := 15;
  Self.Width := 1342;
  Self.Height := 678;

  case Self.Tag of // DO
    0:
      begin
        // Self.Height := 500 - 85;
        cpCompPreset.Visible := False;
        eInput.Visible := True;
        cpProduct0.Visible := False;
        cpProduct1.Visible := False;
        tInputTitle.Caption := frmMain.ReadMessage(52);
        eInput.EmptyText := frmMain.ReadMessage(52);
      end;
    // CustomerCode
    1:
      begin
        // Self.Height := 500 - 85;

        eInput.Visible := True;
        cpCompPreset.Visible := False;
        cpProduct0.Visible := False;
        cpProduct1.Visible := False;
        tInputTitle.Caption := frmMain.ReadMessage(53);
        eInput.EmptyText := frmMain.ReadMessage(53);
      end;
    2: // OrderDetailProduct Retail
      begin
        // Self.Height := 860 - 85;

        cpConfirm.Visible := False;
        eInput.Visible := False;
        cpCompPreset.Visible := False;
        cpProduct1.Visible := False;
        cpProduct0.Visible := True;
        tInputTitle.Caption := frmMain.ReadMessage(54);
        // tProduct0Comp.Caption := frmMain.CompSEQ.ToString;
        tCompTitle.Caption := frmMain.ReadMessage(55);
        t1.Caption := frmMain.ReadMessage(55);
        tProduct0Comp.Caption := frmMain.ODetailSEQ.ToString;
        eInput.EmptyText := frmMain.ReadMessage(54);
      end;
    3: // OrderDetailProduct Jobber
      begin
        // Self.Height := 860 - 85;

        cpCompPreset.Visible := False;
        cpConfirm.Visible := False;
        eInput.Visible := False;
        cpProduct1.Visible := True;
        cpProduct0.Visible := False;
        tInputTitle.Caption := frmMain.ReadMessage(56);
        // tProduct1Comp.Caption := frmMain.CompSEQ.ToString;
        tCompTitle.Caption := frmMain.ReadMessage(55);
        t1.Caption := frmMain.ReadMessage(55);
        tProduct1Comp.Caption := frmMain.ODetailSEQ.ToString;
        eInput.EmptyText := frmMain.ReadMessage(56);
      end;
    4: // OrderDetailPreset
      begin
        // Self.Height := 500 - 85;

        cpCompPreset.Visible := False;
        eInput.Visible := True;
        cpProduct0.Visible := False;
        cpProduct1.Visible := False;
        // tInputTitle.Caption := '�ӹǹ��觫��͹���ѹ - ��ͧ��� ' + frmMain.CompSEQ.ToString;
        tInputTitle.Caption := Format(frmMain.ReadMessage(57), [frmMain.ODetailProName]);
        eInput.EmptyText := Format(frmMain.ReadMessage(57), [frmMain.ODetailProName]);
      end;
    5: // OrderCompPreset
      begin
        // Self.Height := 630 - 85;

        cpCompPreset.Visible := True;
        eInput.Visible := False;
        cpConfirm.Visible := False;
        cpProduct0.Visible := False;
        cpProduct1.Visible := False;
        tInputTitle.Caption := Format(frmMain.ReadMessage(58), [frmMain.CompSEQ.ToString, frmMain.ODetailProName]);
        // tCompTitle.Caption := '��ͧ���';
        eInput.EmptyText := frmMain.ReadMessage(59);
      end;
    6: // SealCount
      begin
        // Self.Height := 500 - 85;

        cpCompPreset.Visible := False;
        eInput.Visible := True;
        cpProduct0.Visible := False;
        cpProduct1.Visible := False;
        tInputTitle.Caption := frmMain.ReadMessage(60);
        eInput.EmptyText := frmMain.ReadMessage(60);
      end;
  end;

  // ShowMessage(Self.Tag.ToString);
end;

end.
