program TASKioskSRT;

{$R *.dres}

uses
  Vcl.Forms,
  UfrmMain in 'UfrmMain.pas' {frmMain},
  UfrmMessage in 'UfrmMessage.pas' {frmMessage},
  UfrmInput in 'UfrmInput.pas' {frmInput},
  UfrmListData in 'UfrmListData.pas' {frmListData},
  UfrmHelp in 'UfrmHelp.pas' {frmHelp};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmMessage, frmMessage);
  Application.CreateForm(TfrmHelp, frmHelp);
  Application.Run;

end.
